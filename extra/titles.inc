<?php

	$headtext = "Nilo's Place - ";
	$CONTENT[titles][message_board] = $headtext."Message Board Forum.";
	$CONTENT[titles][message_board_list] = $headtext."Message Board List.";
	$CONTENT[titles][message_view_thread] = $headtext."Message Board.";
	$CONTENT[titles][message_board_top_threads] = $headtext."Message Board.";
	$CONTENT[titles][message_board_search] = $headtext."Message Board Forum.";
	$CONTENT[titles][message_edit] = $headtext."Message Board.";
	$CONTENT[titles][message_new] = $headtext."Message Board.";
	$CONTENT[titles][classifieds] = $headtext."Classifieds.";
	$CONTENT[titles][classified_new] = $headtext."Classifieds.";
	$CONTENT[titles][login] = $headtext."Login.";
	$CONTENT[titles][download] = $headtext."Downloads.";
	$CONTENT[titles][create_account] = $headtext."Create Account.";
	$CONTENT[titles][preferences] = $headtext."Account Preferences.";
	$CONTENT[titles][my_classifieds] = $headtext."My Classifieds.";
	$CONTENT[titles][message_board_myposts] = $headtext."My Posts.";
	$CONTENT[titles][sauwiki] = "SAU Wiki.";
	$CONTENT[titles][moderators] = $headtext."Moderate This.";
	$CONTENT[titles][message_moderator_info] = $headtext."Moderator Info.";
	$CONTENT[titles][picture_shoot] = $headtext."SAU Pictures.";
	$CONTENT[titles][sharing] = $headtext."Sharing.";

?>
