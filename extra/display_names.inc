<?php

	$CONTENT[display_name][index] = "Home";
	$CONTENT[display_name][info] = "Info";
	$CONTENT[display_name][comments] = "Comments";
	$CONTENT[display_name][download] = "Mp3 Downloads";
	$CONTENT[display_name][sharing] = "Sharing";
	$CONTENT[display_name][dormdownload] = "Dorm Downloads";
	$CONTENT[display_name][uploads] = "Uploads";
	$CONTENT[display_name][chat] = "Chat Applet";
	$CONTENT[display_name][message_board] = "Message Board";
	$CONTENT[display_name][login] = "Login";
	$CONTENT[display_name][logout] = "Logout";
	$CONTENT[display_name][links] = "Links";
	$CONTENT[display_name][burnedvideos] = "Burned Videos";
	$CONTENT[display_name][burnedmp3s] = "Burned Mp3s";
	$CONTENT[display_name][requests] = "Requests";
	$CONTENT[display_name][updates] = "Updates";
	$CONTENT[display_name][icecast] = "Icecast";
	$CONTENT[display_name][preferences] = "My Preferences";
	$CONTENT[display_name][my_classifieds] = "My Classifieds";
	$CONTENT[display_name][classifieds] = "Classifieds";
	$CONTENT[display_name][message_board_myposts] = "My Posts";
	$CONTENT[display_name][sauwiki] = "SAU Wiki";
	$CONTENT[display_name][message_moderators] = "Questionable Posts";
	$CONTENT[display_name][message_moderator_info] = "Moderator Info";
	$CONTENT[display_name][picture_shoot] = "SAU Pictures";
	$CONTENT[display_name][my_pictures] = "My Pictures";
	$CONTENT[display_name][picture_moderate] = "Moderate Pictures";

?>
