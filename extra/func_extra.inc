<?php

global $FUNC_EXTRA_INC;
if (!$FUNC_EXTRA_INC){
$FUNC_EXTRA_INC=1;

	function test_mysql() {
		global $DEBUG;
		if($DEBUG) {
			$con = mysql_connect("localhost", "test", "test");
		} else {
			$con = @mysql_connect("localhost", "test", "test");
		}
		if(!$con) {
			printheader("grey", "This is a test.", 0, 0, 30);
			#print "<BR><BR><CENTER>My Database server is down.<BR>I will have it back up as soon as I am done.<BR>\n";
			print "<BR><BR><CENTER>Nilosplace is getting updated, My Database server is down.<BR>I will have it back up as soon as I am done.<BR>\n";
			printfooter("short");
			exit;
		}
	}

   function convert_mysql_time($time) {
		#print $time."<BR>\n";
      $year = substr($time, 0,4);
      $month = substr($time, 5, 2);
      $day = substr($time, 8, 2);
      $hour = substr($time, 11, 2);
      $minute = substr($time, 14, 2);
      $second = substr($time, 17, 2);
      return mktime($hour, $minute, $second, $month, $day, $year);
   }

	function printtitle($pagetitle, $layout="short", $jsinc = 0, $meta_refresh = 0) {
		global $CONTENT, $INC;
      print "<HTML>\n\t<HEAD>\n\t\t<TITLE>$pagetitle</TITLE>";
      $string = date("D M j g:i:s T Y", filemtime($CONTENT[pages][updates]));
      print "
      <META NAME=\"Generator\" CONTENT=\"Nilo's Web Design.\">
      <META NAME=\"Author\" content=\"Nilo\">
      <META name=\"copyright\" content=\"&copy; Nilo Is Cool\">
      <META name=\"ROBOTS\" content=\"ALL, INDEX\">
      <META NAME=\"title\" CONTENT=\"Nilo's WebSite.\" >
      <META NAME=\"LANGUAGE\" CONTENT=\"en\" >
      <META NAME=\"DOCUMENTCOUNTRYCODE\" CONTENT=\"us\" >
      <meta name=\"keywords\" lang=\"en-us\" content=\"search, find, media, good, data, dorm, download, downloads\">
      <META NAME=\"PUBLISHED_DATE\" CONTENT=\"$string\">
      <meta name=\"description\" content=\"Nilo's WebSite.\">
      <META HTTP-EQUIV=\"published\" CONTENT=\"$string\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">\n";
         
      if($meta_refresh) {
         global $PHP_SELF;
         print "\t\t\t<meta http-equiv=\"REFRESH\" content=\"$meta_refresh; URL=$PHP_SELF\">";
      }  
      if($jsinc > 0) { include($INC[js]); }
		if($jsinc > 1) {
			print "<script src=\"ua.js\"></script>\n";
			print "<script src=\"cp.js\"></script>\n";
			print "<script src=\"ftiens4.js\"></script>\n";
		}
		if($jsinc > 2) {
			global $start, $hits;
			print "<script src=\"message_board_tree_js.php?start=$start&hits=$hits\"></script>";
		}
		
      print "\t</HEAD>\n";
   } // end function printtitle()

	
function printheader($jsinc = 0, $meta_refresh = 0) {
	global $COLORS, $INC, $HTML;
	global $CONTENT, $PHP_SELF;

	$color = $CONTENT[colors][$CONTENT[name][basename($PHP_SELF)]];
	$title = $CONTENT[titles][$CONTENT[name][basename($PHP_SELF)]];
	$layout = $CONTENT[layouts][$CONTENT[name][basename($PHP_SELF)]];
	if(!$layout) $layout = "short";
	print "<!-- Starting Header Include -->";
	printtitle($title, $layout, $jsinc, $meta_refresh);
   print "\t<BODY BGCOLOR=\"#000000\" ALINK=\"#".$COLORS[$color][alink]."\" VLINK=\"#".$COLORS[$color][vlink]."\" LINK=\"#".$COLORS[$color][link]."\" TEXT=\"CCCCCC\">\n";

   if ($layout == "long") {
      print "\t<CENTER>";
      include($INC[header_searchbar]);
      print "\t</CENTER>\n";
   } elseif ($layout) {
      print "\t<CENTER>";
      printoptblob(); 
      //include($INC[header_searchbar]);
      print "\t</CENTER>\n";
	} else {
	   print "<!-- This is nothing -->\n";
	}
	print "<!-- Ending Header Include -->\n";
}


function printoptblob() {

	global $COLORS, $CONTENT, $auth, $admin;

	$list = array("index", "info", "sharing", "chat", "message_board", "sauwiki", "picture_shoot", "classifieds", "login");
	for($i = 0; $i < count($list); $i++) {
		print_link($i, $CONTENT[pages][$list[$i]], $COLORS[$CONTENT[colors][$list[$i]]], $CONTENT[display_name][$list[$i]]);
	}

	if(session_is_registered("admin") && $admin == 1) {
		$list = array("download", "icecast", "edit_post", "uploads", "dormdownload");
		print "<BR><BR><b>Admin:</b> ";
		for($i = 0; $i < count($list); $i++) {
			print_link($i, $CONTENT[pages][$list[$i]], $COLORS[$CONTENT[colors][$list[$i]]], $CONTENT[display_name][$list[$i]]);
		}
	}

	if(session_is_registered("auth") && $auth == 1) {
		$list = array("my_pictures", "my_classifieds", "message_board_myposts", "preferences", "logout");
		print "<BR><BR>";
		for($i = 0; $i < count($list); $i++) {
			print_link($i, $CONTENT[pages][$list[$i]], $COLORS[$CONTENT[colors][$list[$i]]], $CONTENT[display_name][$list[$i]]);
		}
	}

	if(session_is_registered("modarray") && session_is_registered("auth") && $auth == 1) {
		$list = array("picture_moderate", "message_moderator_info", "message_moderators");
		print "<BR><BR>";
		for($i = 0; $i < count($list); $i++) {
			print_link($i, $CONTENT[pages][$list[$i]], $COLORS[$CONTENT[colors][$list[$i]]], $CONTENT[display_name][$list[$i]]);
		}
	}

/*
	print "
        <A HREF=\"".$CONTENT[pages][index]."\"><FONT color=#".$COLORS[$CONTENT[colors][index]][link]."><nobr>Home</nobr></FONT></A>
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"".$CONTENT[pages][info]."\"><FONT color=#".$COLORS[$CONTENT[colors][info]][link]."><nobr>Info</nobr></FONT></A> -->
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"".$CONTENT[pages][comments]."\"><FONT color=#".$COLORS[$CONTENT[colors][comments]][link]."><nobr>Comments</nobr></FONT></A> -->
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"".$CONTENT[pages][download]."\"><FONT color=#".$COLORS[$CONTENT[colors][download]][link]."><nobr>Mp3 Downloads</nobr></FONT></A> -->
        <B>|&nbsp;</B><A HREF=\"".$CONTENT[pages][dormdownload]."\"><FONT color=#".$COLORS[$CONTENT[colors][dormdownload]][link]."><nobr>Dorm Downloads</nobr></FONT></A>
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"".$CONTENT[pages][uploads]."\"><FONT color=#".$COLORS[$CONTENT[colors][uploads]][link]."><nobr>Uploads</nobr></FONT></A>-->
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"".$CONTENT[pages][chat]."\"><FONT color=#".$COLORS[$CONTENT[colors][chat]][link]."><nobr>Chat Applet</nobr></FONT></A> -->
        <B>|&nbsp;</B><A HREF=\"".$CONTENT[pages][message_board]."\"><FONT color=#".$COLORS[$CONTENT[colors][message_board]][link]."><nobr>Message Board</nobr></FONT></A>
        <B>|&nbsp;</B><A HREF=\"".$CONTENT[pages][login]."\"><FONT color=#".$COLORS[$CONTENT[colors][login]][link]."><nobr>Login</nobr></FONT></A>
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"".$CONTENT[pages][links]."\"><FONT color=#".$COLORS[$CONTENT[colors][links]][link]."><nobr>Links</nobr></FONT></A> -->
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"".$CONTENT[pages][burnedvideos]."\"><FONT color=#".$COLORS[$CONTENT[colors][burnedvideos]][link]."><nobr>Burned Videos</nobr></FONT></A> -->
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"".$CONTENT[pages][burnedmp3s]."\"><FONT color=#".$COLORS[$CONTENT[colors][burnedmp3s]][link]."><nobr>Burned Mp3s</nobr></FONT></A> -->
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"http://www.jokerdate.com\"><FONT color=#".$COLORS[$CONTENT[colors][comments]][link]."><nobr>JokerDate</nobr></FONT></A> -->
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"http://www.jokervote.com\"><FONT color=#".$COLORS[$CONTENT[colors][burnedmp3s]][link]."><nobr>JokerVote</nobr></FONT></A> -->
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"".$CONTENT[pages][requests]."\"><FONT color=#".$COLORS[$CONTENT[colors][requests]][link]."><nobr>Requests</nobr></FONT></A> -->
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"".$CONTENT[pages][updates]."\"><FONT color=#".$COLORS[$CONTENT[colors][updates]][link]."><nobr>Updates</nobr></FONT></A> -->
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"/mailman/listinfo/mp3s\"><FONT color=#".$COLORS[magenta][link]."><nobr>Subscribe</nobr></FONT></A> -->
        <!-- <B>&nbsp;|&nbsp;</B><A HREF=\"".$CONTENT[pages][icecast]."\"><FONT color=#".$COLORS[$CONTENT[colors][icecast]][link]."><nobr>Icecast</nobr></FONT></A> -->";
*/

}

	function print_link($head, $url, $color, $name) {
		if($head) print "<B>|&nbsp;</B>";
		print "<A HREF=\"$url\"><FONT color=#$color[link]><nobr>$name</nobr></FONT></A>\n";
	}


	function southern_ip() {
		global $REMOTE_ADDR;
		$ipn = $REMOTE_ADDR;
	   $ip = split("\.", $ipn);
		if($ipn == "192.168.2.3") return 1;
		if($ip[0] == "216" && $ip[1] == "229" && $ip[2] >= "224" && $ip[2] <= "239") return 1;
		else return 0;
	}


	function access() {
		global $REMOTE_ADDR;
		$ip = split("\.", $REMOTE_ADDR);

		if($REMOTE_ADDR == "192.168.2.3") return 1;
		if($REMOTE_ADDR == "66.112.37.252") return 1;
		#if($testhour >= 0 && $testhour < 4) $houris = "good";
		if($ip[0] == "216" && $ip[1] == "229" && $ip[2] >= "224" && $ip[2] <= "231") $ipis = "admin";
		if($ip[0] == "216" && $ip[1] == "229" && $ip[2] >= "232" && $ip[2] <= "239") $ipis = "dorm";
	   //if ($houris == "good" && $ipis != "admin") return 1;
		if ($ipis == "dorm") return 1;
		else return 0;
	}


	function sendfile($fullpath) {
		global $dbfunctions, $debug, $REMOTE_ADDR, $INC;
		include($INC[download_log]);
		$log = new DOWNLOADLOG($debug);
		set_time_limit(3600);
      if (!(@is_file($fullpath) && filesize($fullpath) > 0)) return 0;

		$filename=basename($fullpath);
		$log->insert($REMOTE_ADDR, $filename); 
		header("Content-Type: application/octet-stream");
		$lengthstring = "Content-Length: ".filesize($fullpath);
		header($lengthstring);
		header(  "Content-Disposition: \"inline; filename=$filename\"");
		#header(  "Content-Disposition: \"attachment; filename=$filename\"");
		header( "Content-Description: \"$filename\"" );
		if ($bytes = readfile($fullpath)) {
			//$fh = fopen("/tmp/log_mp3s", "w");
			//fputs($fh, $bytes);
			//fclose($fh);
			return 1;
		} else return 0;
	}

	function printcounter() {
		global $log;
		if($log) {
			$array = $log->get_stats();
			$num = $array[sum];
		}
		else {
			global $INC, $debug;
			global $dbfunctions, $debug;
			include($INC[logs]);
			$log = new LOGS($debug);
			$array = $log->get_stats();
			$num = $array[sum];
		}
	   for ($i = 0; $i < strlen($num); $i++) {
	       $imgstring .=  "\n         <TD><IMG SRC=pix/".substr($num, $i, 1).".jpg ALT=\"".substr($num, $i, 1)."\"></TD>";
	   }
		print  "<CENTER>\n   <TABLE border=0>\n      <TR>$imgstring\n      </TR>\n   </TABLE></CENTER>\n";
	}


	function printfooter($display = "") {
		global $CONTENT, $ADMINNAME, $ADMINEMAIL1, $HTML;
			print "<CENTER>\n";
		if ($display == "long") {
			$LastAccess = filemtime($CONTENT[pages][updates]);
			# print (date("F d, Y", mktime(14,0,0,1,1,2000)));
			print "<BR>\n<BR>\nLast <A HREF=\"".$CONTENT[pages][updates]."\">updated</A> on ".date("l F d, Y", $LastAccess)."<BR><BR>\n";
			print "Number of hits to this site.<BR>";
			printcounter();
			print "<BR><BR><a href=\"https://www.paypal.com/xclick/business=nilo%40nilosplace.net&item_name=Server+Upgrade+Cost&no_shipping=1&return=http%3A//mp3s.nilosplace.net/thanks.php\" alt=\"Donate Money to Nilo\"><IMG src=\"http://images.paypal.com/images/x-click-but04.gif\" border=\"0\" alt=\"Donate\"></a><BR>\n";
			$display = "banner";
		}
		global $sitelist;
		print "<!-- Starting Footer Include -->\n";
		print "   <BR>\n   Brought to you by: <A HREF=\"mailto:$ADMINEMAIL1\">$ADMINNAME</A><BR>\n";
		//for($i = 0; $i < count($sitelist); $i++) {
		//	print "<font size=+3 color=000000><a href=\"http://$sitelist[$i]\" style=\"color: #000000\">.</a></font>\n";
		//}
		global $adds;
		if($display == "banner" && $adds) {
			print "\t<BR><BR><CENTER>";
			include($HTML[banner]);
			print "\t</CENTER>";
		}

		print " </BODY>\n</HTML>\n";
		print "<!-- Ending Footer Include -->\n";
	} // end function printfooter();

	function printlife() {
		global $CONTENT;
		$birth=gmmktime(20,17,0,10,22,1978);
		$current = time();
		$seconds=$current-$birth;
		$days=$seconds/86400;
		$seconds=$seconds%86400;
		$hours=$seconds/3600;
		$seconds=$seconds%3600;
		$minutes=$seconds/60;
		$seconds=$seconds%60;
		printf("Nilo has been alive for %d days, %d hours, %d minutes, and %d seconds.<BR>\n",$days,$hours,$minutes,$seconds );
		print "For anyone of you that care to see more click <A HREF=\"".$CONTENT[pages][more]."\">here</A>\n";
	}

function mailerror($problemstring, $filename, $errortype) {
global $REMOTE_ADDR;
global $DATE;
global $ADMINEMAIL1;
global $maclines;
$problemstring .= "\n";
$hostname = gethostbyaddr($REMOTE_ADDR);
$ips = split("\.", $REMOTE_ADDR);
if ($ips[0] == "216" && $ips[1] == "229" && $ips[2] >= 232 && $ips[2] <= 239) {
	exec("/usr/bin/getstuff $REMOTE_ADDR", $maclines);
		for($i = 0; $i < count($maclines); $i++) {
			$problemstring .= $maclines[$i]."\n";
		}
}
switch ($errortype) {
		case ("usage"):
			$errormessage = "Person trying to get in.";
			break;
		case ("file"):
			$errormessage = "Serious file problem.";
			break;
}

mail($ADMINEMAIL1,$errormessage,
"
$problemstring
Filename = $filename
Date = $date
Ip = $REMOTE_ADDR
Hostname = $hostname
"
, "From: web@nilosplace.net\nContent-Type: text/html; charset=iso-8859-1");
exit;
}


} // end include protection

?>
