<?php

$COLORS[realdarkblue][alink] = "0000C0";
$COLORS[realdarkblue][link] = "0000FF";
$COLORS[realdarkblue][vlink] = "000080";
$COLORS[darkblue][alink] = "0060C0";
$COLORS[darkblue][link] = "0080FF";
$COLORS[darkblue][vlink] = "004080";
$COLORS[blue][link] = "0000FF";
$COLORS[blue][alink] = "0000C0";
$COLORS[blue][vlink] = "000080";
$COLORS[green][alink] = "00C000";
$COLORS[green][link] = "00FF00";
$COLORS[green][vlink] = "008000";
$COLORS[lightgreen][alink] = "00C060";
$COLORS[lightgreen][link] = "00FF80";
$COLORS[lightgreen][vlink] = "008040";
$COLORS[lightblue][alink] = "00C0C0";
$COLORS[lightblue][link] = "00FFFF";
$COLORS[lightblue][vlink] = "008080";
$COLORS[darkpurple][alink] = "6000C0";
$COLORS[darkpurple][link] = "8000FF";
$COLORS[darkpurple][vlink] = "400080";
$COLORS[darkgreen][alink] = "60C000";
$COLORS[darkgreen][link] = "80FF00";
$COLORS[darkgreen][vlink] = "408000";
$COLORS[red][alink] = "C00000";
$COLORS[red][link] = "FF0000";
$COLORS[red][vlink] = "800000";
$COLORS[magenta][alink] = "C00060";
$COLORS[magenta][link] = "FF0080";
$COLORS[magenta][vlink] = "800040";
$COLORS[purple][alink] = "C000C0";
$COLORS[purple][link] = "FF00FF";
$COLORS[purple][vlink] = "800080";
$COLORS[orange][alink] = "C06000";
$COLORS[orange][link] = "FF8000";
$COLORS[orange][vlink] = "804000";
$COLORS[yellow][alink] = "C0C000";
$COLORS[yellow][link] = "FFFF00";
$COLORS[yellow][vlink] = "808000";
$COLORS[grey][alink] = "CCCCCC";
$COLORS[grey][link] = "FFFFFF";
$COLORS[grey][vlink] = "999999";
$COLORS[lime][link] = "80FF80";
$COLORS[lime][alink] = "00C000";
$COLORS[lime][vlink] = "408040";


?>
