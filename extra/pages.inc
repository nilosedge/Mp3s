<?php

$CONTENT[colors][comments] = "yellow";
$CONTENT[colors][download] = "green";
$CONTENT[colors][sharing] = "green";
$CONTENT[colors][dormdownload] = "darkblue";
$CONTENT[colors][index] = "red";
$CONTENT[colors][burnedmp3s] = "blue";
$CONTENT[colors][burnedvideos] = "blue";
$CONTENT[colors][info] = "orange";
$CONTENT[colors][links] = "lightblue";
$CONTENT[colors][login] = "lightblue";
$CONTENT[colors][logout] = "lightblue";
$CONTENT[colors][create_account] = "lightblue";
$CONTENT[colors][preferences] = "grey";
$CONTENT[colors][my_classifieds] = "magenta";
$CONTENT[colors][classifieds] = "magenta";
$CONTENT[colors][classified_view] = "magenta";
$CONTENT[colors][classified_new] = "magenta";
$CONTENT[colors][polls] = "darkblue";
$CONTENT[colors][requests] = "purple";
$CONTENT[colors][icecast] = "grey";
$CONTENT[colors][result] = "lightgreen";
$CONTENT[colors][updates] = "darkpurple";
$CONTENT[colors][uploads] = "darkgreen";
$CONTENT[colors][window] = "";
$CONTENT[colors][chat] = "grey";
$CONTENT[colors][message_board] = "lime";
$CONTENT[colors][message_board_list] = "lime";
$CONTENT[colors][message_edit] = "grey";
$CONTENT[colors][message_view] = "lime";
$CONTENT[colors][message_view_thread] = "lime";
$CONTENT[colors][message_new] = "lime";
$CONTENT[colors][message_board_top_threads] = "lime";
$CONTENT[colors][message_board_search] = "lime";
$CONTENT[colors][message_board_stats] = "lime";
$CONTENT[colors][message_board_tree] = "lime";
$CONTENT[colors][message_board_myposts] = "lime";
$CONTENT[colors][sauwiki] = "purple";
$CONTENT[colors][message_moderators] = "lime";
$CONTENT[colors][message_moderator_info] = "lime";
$CONTENT[colors][picture_shoot] = "grey";
$CONTENT[colors][my_pictures] = "grey";
$CONTENT[colors][picture_moderate] = "grey";

$CONTENT[pages][sauwiki] = "http://sauwiki.com";

if(is_dir($WEBSOURCE)) {
	if($htmld = opendir($WEBSOURCE)) {
		while($file = readdir($htmld)) {
			if(!is_dir($WEBSOURCE."/".$file)) {
				preg_match("/([^\.]*)\.php$/", $file, $match);
				if($match[1]) {
					$PAGES[$match[1]] = $match[0]; 
					$CONTENT[pages][$match[1]] = $match[0];
					$CONTENT[name][$match[0]] = $match[1];
					$PAGE_COLORS[$CONTENT[pages][$match[1]]] = $CONTENT[colors][$match[1]];
				}
			}
		}
	} else {
		print "Counld not find htmldir $HTMLDIR, quiting.<BR>\n";
		exit;
	}
} else {
	print "Counld not find htmldir $HTMLDIR, quiting.<BR>\n";
	exit;
}

?>
