<?php

$inc_array = array(	"request",
							"mp3s",
							"top_downloaded",
							"download_log",
							"playlist",
							"playing",
							"logs",
							"updates",
							"super_admins",
							"users",
							"message_board",
							"Classifieds",
							"Categories",
							"Pictures",
							"moderators",
							"moderated"
						);

for($i = 0; $i < count($inc_array); $i++) {
	$include = $inc_array[$i];
	if($debug) print "This is the include $include<BR>\n";
	$INC[$include] = "$PHPINC/func_$DBNAME"."_tbl_$include.inc";
}

$dorm_inc_array = array(	"dorm",
									"dorminfo",
									"dormlog",
									"poll_questions",
									"poll_answers",
									"poll_votes",
									"skipwords",
									"hosts",
									"shares",
									"dirs"
								);


for($i = 0; $i < count($dorm_inc_array); $i++) {
	$include = $dorm_inc_array[$i];
	if($debug) print "This is the include $include<BR>\n";
	$INC[$include] = "$PHPINC/func_dorm_tbl_$include.inc";
}

$INC[db] = "$PHPINC/func_db_mysql.inc";
$INC[extra] = "$EXTRA/func_extra.inc";
$INC[header_searchbar] = "$EXTRA/header_searchbar.inc";
$INC[parser] = "$PHPINC/func_class_parse_token.inc";
$INC[js] = "$JSINC/func_js.inc";
$INC[mp3info] = "$EXTRA/mp3info.inc";
?>
