<?php


include("../config.inc");

$array = array("extra", "dorm", "dormlog", "updates", "logs", "dorminfo", "poll_questions", "poll_answers");
for($i = 0; $i < count($array); $i++) { 
	if($debug) print $INC[$array[$i]]."<BR>\n";
	include($INC[$array[$i]]); 
}

$log = new LOGS($debug);
$log->update($REMOTE_ADDR, $HTTP_USER_AGENT);
$jsinc = 1;
$logs = new DORMLOG($debug);
$running_index = $logs->run_index();


printheader($jsinc, $running_index);

if($running_index) {
	$info_array = $logs->get_all();

	print "<BR><BR><CENTER><H1>RUNNING Index</H1>\n";

	$running_time = $logs->get_running_time();
	$start_time = $logs->get_start_time();
	//	$temp = $start_time + $running_time;
	$minutes = floor($running_time / 60);
	$running_time -= $minutes*60;
	$seconds = $running_time;
	$total_file_count = $logs->get_total_file_count();
	$current_file_count = $logs->get_current_file_count();
	$start_text = date("h:i:s", $start_time);
	//	$end_text = date("h:i", $temp);
	$current_time = time();
	$current_text = date("h:i:s", $current_time);

	$running_time = $current_time - $start_time;
	$percent = floor($current_file_count/$total_file_count*100);
#	$end_time = (100 * $running_time)/$percent;
	$end_text = date("h:i:s", ($start_time + $end_time));
	$file_rate = number_format($current_file_count / ($current_time - $start_time));

	print "I am currently INDEXING $total_file_count files, started at $start_text<BR>\n";
	print "Last index running time was $minutes minutes and $seconds seconds.<BR><BR>";
#	print "Completed: $percent%<BR><BR>\n";
#	print "Current rate is $file_rate files per second<BR><BR>\n";
	print "Current system time is $current_text<BR><BR>\n";
#	print "Estimated finish time $end_text\n";
#	print "<BR><BR>";


#	$total_size = $logs->get_total_size();
#	print "Total Sharing Size: ".number_format($total_size / (1024 * 1024 * 1024))." GB<BR>\n";

#	$logs->print_top20_files();

}
else {
	$poll_q = new POLL_QUESTIONS($debug);
	$poll_a = new POLL_ANSWERS($debug);

	$q_ar = $poll_q->get_latest();
	#print "<BR><BR><CENTER>This Page died over break I have not had time to figure out what went wrong but it will be up by the end of the week.";
	$stop_time = $logs->get_stop_time();
	print "<BR><CENTER>Last indexed: ".date("l F j, H:i", $stop_time)."<BR>\n";
	$logs = new DORMLOG($debug);
	$info = new DORMINFO($debug);
	$infoblob = $info->get_lastest();

	if($SIP || $admin || (session_is_registered("auth") && $auth == 1)) {
		include($HTML[dormsearch]);
	}
	else include($HTML[dormdown]);
	//include($HTML[dormsearch]);
}
printfooter("short");
?>
