<?php

	include("../config.inc");
	include($INC[message_board]);
	include($INC[logs]);
	include($INC[extra]);
	//session_start();
	// $debug = $DEBUG = 1;
	$log = new LOGS($debug);
	$log->update($REMOTE_ADDR, $HTTP_USER_AGENT);

	$message = new message_board($DEBUG);

	if(session_is_registered("auth") && $auth == 1) {
		printheader();
		include($CONTENT[html][message_board_myposts]);
		printfooter("banner");
	} else {
		header("Location: $PAGES[login]");
	}


?>
