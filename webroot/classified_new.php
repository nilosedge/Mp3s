<?php

	include("../config.inc");
	include($INC[Classifieds]);
	include($INC[Categories]);

	if(session_is_registered("auth") && $auth == 1) {

	$class = new Classifieds($DEBUG);
	$cats = new Categories($DEBUG);

	if($create_button && $array[SmallDescription] && $array[CategoryId]) {
		$array[SmallDescription] = strip_tags($array[SmallDescription]);
		$array[LargeDescription] = strip_tags($array[LargeDescription]);
		$array[Price] = strip_tags($array[Price]);
		$array[WebSite] = strip_tags($array[WebSite]);
		$array[Picture] = strip_tags($array[Picture]);
		$class->InsertClass($array);
		header("Location: ".$CONTENT[pages][my_classifieds]);
	} elseif($update_button) {
		$class->ModifyClass($username, $array);
		header("Location: ".$CONTENT[pages][my_classifieds]);
	} elseif($delete_button && $array[id] && $action == "Yes") {
		if(!$class->DeleteClass($username, $array[id])) {
			printheader();
			print "<BR><BR><BR><center><font size=+3>Deletion Successful</font>";
			printfooter();
		} else {
			printheader();
			print "<BR><BR><BR><center><font size=+3>Deletion UnSuccessful</font>";
			printfooter();
		}
	} elseif($delete_button && $array[id]) {
		printheader();
		$array = $class->get_info($array[id]);
		print "<CEnter><BR><BR><BR><font size=+3>
			Are you sure you want to delete $array[SmallDescription]?</font>
			<table><tr><td><form action=\"$PHP_SELF\" method=post>
				<input type=hidden name=delete_button value=1>
				<input type=hidden name=array[id] value=$array[id]>
				<input type=submit name=action value=\"Yes\">
			</form></td><td><form action=\"".$CONTENT[pages][my_classifieds]."\"><input type=submit name=no_button value=\"No\"></form></td></tr></table><BR><BR><BR>
		";
		printfooter();
	} elseif($edit_button && $array[id]) {
		$array = $class->get_info($array[id]);
		printheader();
		include($CONTENT[html][$CONTENT[name][basename($PHP_SELF)]]);
		printfooter();
	} elseif($cancel_button) {
		header("Location: ".$CONTENT[pages][my_classifieds]);
	} else {
		printheader();
		include($CONTENT[html][$CONTENT[name][basename($PHP_SELF)]]);
		printfooter();
	}

	} else {
		header("Location: ".$CONTENT[pages][logout]);
	}

?>
