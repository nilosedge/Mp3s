<?php

	include("../config.inc");

	include($INC[Classifieds]);
	
	$class = new Classifieds($DEBUG);

	if(session_is_registered("auth") && $auth == 1) {
		printheader();
		include($HTML[my_classifieds]);
		printfooter("banner");
	} else {
		header("Location: $PAGES[login]");
	}



?>
