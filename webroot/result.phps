<?php
include("../config.inc");
$array = array("extra", "mp3s", "playlist", "dormlog", "dorm");
for($i = 0; $i < count($array); $i++) { include($INC[$array[$i]]); }

#include("$functions/func_extra.inc");
#include("$dbfunctions/func_table_mp3s.inc");
#include("$dbfunctions/func_table_dormlog.inc");
#include("$dbfunctions/func_table_playlist.inc");
#include("$dbfunctions/func_table_dorm.inc");

#session_start();
#$DEBUG = $debug = 1;

$search = new MP3($debug);

$urlsearchfor = urlencode($searchfor);


if($playlist) {
	$list = new Playlist($debug);
	if(southern_ip()) $level = 1;
	else $level = 0;

	if($admin) $level = 2;
	
	for(reset($playlist); $id = key($playlist); next($playlist)) {
		$list->insert($id, $level);
	}
	if($lsd) { $lsd = rawurlencode($lsd); $PHP_SELF = $CONTENT[pages][download]."?lsd=$lsd"; }
	else $PHP_SELF = "$PHP_SELF?hits=$hits&start=$start&searchfor=$urlsearchfor&searchby=$searchby";
	$CONTENT[titles][result] = "Nilo's Mp3s - PlayList Updated";
	$CONTENT[colors][result] = "grey";
	printheader(0, 1);
	print "<center><font size=+2><BR><BR><BR>That song was added to the Nilosplace Radio Playlist.<BR><BR><BR>\n";
	printfooter("short");
}
else if (isset($searchfor) && !$random && !$download && !$dorm) {
	if (!$searchby) $searchby = "all";
	$CONTENT[titles][result] = "Nilo's Mp3s - Search Results for: $searchfor";
	$CONTENT[colors][result] = "green";
   printheader();
	if($admin == 0 && !access()) $searchfor = "_-===-_";
	$search->search($searchfor, $searchby, $start, $hits);
	print "<CENTER>\n";		
	$search->display($layout);
	print "</CENTER>\n\n";		
	printfooter("short");
}
//elseif ((access(Date("H"),$REMOTE_ADDR) || $auth) && ($random || $download)) {
elseif ((access() || $auth || $admin) && ($random || $download)) {
	if ($random) $search->randomdownload();
	elseif($download) $search->download($download);
}
elseif (!($auth) && ($download || $random)) {
   header("Location: $WEBSITEURL/".$CONTENT[pages][login]."?download=$download&random=$random");
   mailerror("Someone trying to get in during the day.","Someone searched and now is trying to download.","usage");
}
elseif ($dorm) {
	$CONTENT[titles][result] = "Nilo's Mp3s - Dorm Search Results for: $searchfor";
	$CONTENT[colors][result] = "darkblue";
	printheader();
	$dorm_search = new DORM($debug);
	$log = new DORMLOG($debug);
	$dorm_search->search($searchfor, $start, $hits);
	$log->store($searchfor, $dorm_search->totalhits, $REMOTE_ADDR);
	print "<CENTER>\n";
	$dorm_search->display($layout);
	print "</CENTER>\n\n";
	printfooter("short");
}
else header("Location: $WEBSITEURL");
exit;
?>
