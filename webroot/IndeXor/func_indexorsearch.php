<?php

global $FUNC_INDEXORSEARCH_INC;
if (!$FUNC_INDEXORSEARCH_INC){
   $FUNC_INDEXORSEARCH_INC=1;

include("func_db_mysql.php");
include("func_parse_token.php");

class IndeXorSearch {

   var $db, $resultid, $searchquery, $table; 
   var $searchstring, $totalhits;
   var $hits, $start, $debug;

   function IndeXorSearch($debug){
      global $INDEXOR_DBNAME, $INDEXOR_DBUSER, $INDEXOR_DBHOST, $INDEXOR_DBPASSWD;
      $this->debug = $debug;
      $this->table = "Files";
      $this->db = new Database($debug);
      $this->db->connect($INDEXOR_DBHOST, $INDEXOR_DBUSER, $INDEXOR_DBPASSWD);
      $this->db->usedatabase($INDEXOR_DBNAME);
   }


   function search($searchstring, $start, $hits) {
      $searchstring = stripslashes($searchstring);
      $searchstring = str_replace("*", "%", $searchstring);
      $this->searchstring = $searchstring;
      if (!$start) $start = 0;
      if (!$hits) $hits = 10;
      $this->start = $start;
      $this->hits = $hits;

      include("func_indexor_tbl_skipwords.inc");

      $skip = new SkipWords($this->debug);


      $tok = new parse_token($this->searchstring);

      $array = $skip->chop_words($tok->get_array());

      # $array = split(" ", $this->searchstring);
      $keyword = 2;
/*
      if($array && $keyword == 0) {
         $query = "select * ";
         $totalquery = "select count(*) as count ";
         $query .= "from $this->table where ";
         $totalquery .= "from $this->table where ";
         for($i = 0; $i < count($array); $i++) {
            if(!$array[$i]) continue;
            $query .= "concat(FilePath,FileName) like '%$array[$i]%' ";
            $totalquery .= "concat(FilePath,FileName) like '%$array[$i]%' ";
            if($i < (count($array) - 1)) { $query .= " && "; $totalquery .= " && "; }
         }
         $query .= " order by HostId limit $this->start, $this->hits";

         if($this->debug) print "Query: $query<BR>\n";

         $array = $this->db->get_array($this->db->query($totalquery));
         $this->totalhits = $array[count];
         # $this->totalhits = `/bin/cat $filelist | $grep | /usr/local/bin/lines`;
         if($this->totalhits > 0) {
            $this->resultid = $this->db->query($query);      
         } else $this->resultid = 0;
      }
*/
/*
      if($array && $keyword == 1) {
         $query = 'select Hosts.Name as host_name, concat(\'file://\', Hosts.Ip, \'/\', shares.name, \'/\', files.file_path) as path, files.file_name, files.size, files.extention ';
         $totalquery = "select count(*) as count ";
         $query       .= "from $this->table, Keyword, Hosts where Files.Id=Keyword.FileId and Files.share_id=shares.id and files.host_id=hosts.id and hosts.name !='' and (";
         $totalquery  .= "from $this->table, Keyword, Hosts where Files.Id=Keyword.FileId and Files.share_id=shares.id and files.host_id=hosts.id and hosts.name !='' and (";
   
         for($i = 0; $i < count($array); $i++) {
            // if(!$array[$i]) continue;
            $query .= "word = '$array[$i]' ";
            $totalquery .= "word = '$array[$i]' ";
            if($i < (count($array) - 1)) { $query .= " or "; $totalquery .= " or "; }
         }
         $query .= ") group by file_id having count(*) > ".(count($array) - 1)." limit $this->start, $this->hits";
         $totalquery .= ") group by file_id having count(*) > ".(count($array) - 1);

         #if($this->debug) print "Query: $query<BR>\n";

         $res = $this->db->query($totalquery);
         #$array = $this->db->get_array($this->db->query("select ".$this->db->num_rows($res)." as count"));
         #$this->totalhits = $array[count];
         $this->totalhits = $this->db->num_rows($res);

         # $this->totalhits = `/bin/cat $filelist | $grep | /usr/local/bin/lines`;
         if($this->totalhits > 0) {
            $this->resultid = $this->db->query($query);      
         } else $this->resultid = 0;
      }
*/
      if($array && $keyword == 2) {
         $query = 'select FileId from Keywords where (';
         $totalquery = 'select FileId from Keywords where (';

         for($i = 0; $i < count($array); $i++) {
            // if(!$array[$i]) continue;
            $query .= "KeyWord = '$array[$i]' ";
            $totalquery .= "KeyWord = '$array[$i]' ";
            if($i < (count($array) - 1)) { $query .= " or "; $totalquery .= " or "; }
         }
         $query .= ") group by FileId having count(*) > ".(count($array) - 1)." limit $this->start, $this->hits";
         $totalquery .= ") group by FileId having count(*) > ".(count($array) - 1);

         $this->totalhits = $this->db->num_rows($this->db->query($totalquery));

         //print $this->totalhits."<BR>\n";
         $res = $this->db->query($query);

         //$query = 'select Hosts.Name as host_name, concat(\'file://\', Hosts.Ip, \'/\', Files.FilePath) as path, Files.FileName, Files.Size ';
         $query = 'select Hosts.Name as host_name, concat(Files.FilePath) as path, Files.FileName, Files.Size ';
         $query .= "from Hosts, Files where Files.HostId=Hosts.Id and Hosts.Name !='' and (";
         for($count = 0; $a = $this->db->get_array($res); $count++) {
            $query .= "Files.Id = $a[FileId] ";
            if($count < $this->hits - 1 && ($count < ($this->totalhits - $this->start) - 1)) { $query .= " or "; }   
         }
         $query .= ")";

         $this->resultid = $this->db->query($query);

      }

   }

   function display($layout){
      if (!($layout)) { $layout = "long"; }
      if ($layout == "long") {
         if(!$this->resultid) $numresult = 0;
         else $numresult = $this->db->num_rows($this->resultid);

         print "<!-- Starting Table Generation -->\n";
         if ($numresult) {
            if (($this->start + $this->hits) > ($this->start + $numresult)) {
               $temp = $this->start + $numresult;
            }
            else { $temp = $this->start + $this->hits; }
            $bgcolor = "FFFFFF";
            print "<BR>\n";
            print "IndeXor results ".($this->start + 1)." - ".($temp)." of $this->totalhits total matches for: $this->searchstring<BR><BR>\n";
            //print "<BR><font size=+7 color=dd0000>This data is DEAD data. It will NOT work. It's only here for testing purposes.</font><BR><BR>\n";
            //print "If you are looking to download click <a href=\"search.php\">here</a> and look under the Mirrors section for other peoples sites that Index the dorm.<BR>";
            print "<BR><font size=+2 color=dd0000>This is LIVE data,</font> all of it is gathered from the internet.<BR>\n";
            for($step = 0; $step < $numresult; $step++){
               if( !$step ) {
                  print "       <TABLE bgcolor=$bgcolor width=100% border=1 cellpadding=1 cellspacing=1>
         <TR ALIGN=CENTER>
            <TD>Download</TD>
            <TD><U>Host</U></TD>
            <TD bgcolor=ffffff>_</TD>
            <TD><U>Path</U></TD>
            <TD>Explore</TD>
            <TD><U>Size in Bytes</U></TD>
         </TR>";
               }
               $res1 = $this->db->get_array($this->resultid);
               $hostname = $res1[host_name];
               $ip = $res1[Ip];
               $folder = str_replace("smb://", "file://", $res1[path]);
               $full_path = $folder.$res1[FileName];
               $res1[extention] = array_pop(explode('.', $full_path));
               print "
         <TR>
           <TD><A HREF=\"$full_path\" target=new>Download</A></TD>
           <TD>$hostname</TD>
           <TD bgcolor=ffffff>";
           if(@is_file("pix/$res1[extention].gif")) print "<img src=\"pix/$res1[extention].gif\" alt=\"\">";
           else print "<img src=\"pix/h_default.gif\" alt=\"Default\">";
           print "</td>
           <TD>$full_path</TD>
           <TD><A HREF=\"$folder\" target=new><img src=\"pix/explore_c.gif\" border=0 alt=\"Explore\"></A></TD>
           <TD ALIGN=RIGHT>$res1[Size]</TD>
         </TR>\n";
      }
      print "       </TABLE>\n";
      $this->displaynumbers();
      }
      else {
         print "Sorry was unable to find anything that matched your request<BR>\n";
         $this->displaysearch();
      }
      print "<!-- Ending Table Generation -->\n";
    }

    else {
            print "<FONT COLOR=#00FF00>No layout or unknown layout: $layout";       
    }




   } 

   function displaysearch() {
      global $PHP_SELF, $PAGES;
      print "
       <FORM method=GET action=\"$PHP_SELF\">
         <FONT color=\"000000\">
         <INPUT type=text name=searchfor size=31 maxlength=256 value=\"".htmlentities(stripslashes($this->searchstring), ENT_COMPAT)."\"> 
         <INPUT type=hidden name=hits value=\"$this->hits\">
         <INPUT name=search type=submit value=\"IndeXor Search\"></FONT>
       </FORM>\n";
   }

   function get_sharename($id) {
      $array = $this->db->get_assarray($this->db->query("select name from shares where id=\"$id\""));
      return $array[name];
   }

   function get_hostname($id) {
      $array = $this->db->get_assarray($this->db->query("select name from hosts where id=\"$id\""));
      return $array[name];
   }
   function get_hostip($id) {
      $array = $this->db->get_assarray($this->db->query("select ip from hosts where id=\"$id\""));
      return $array[ip];
   }

   function displaynumbers() {
		global $numstyl;
      print "       <BR><BR>\n       <CENTER>\n";
      print "       <TABLE>\n         <TR>\n";
      if ($this->totalhits > $this->hits) {
         if ($this->start > 0) {
            print "           <TD><A HREF=\"search.php?searchfor=".rawurlencode($this->searchstring)."&start=".($this->start - $this->hits)."&hits=$this->hits\"><--Prev</A></TD>\n";
         } else print "           <TD><FONT color=FFFFFF><--Prev</FONT></TD>\n";
         $counter = 0;
         for($i = 0; $i < $this->totalhits && $counter < 20; $i += $this->hits) {
            $counter++;
            $color = $COLORS[darkblue][link];
            print "<td>\n";
            if($i == $this->start) print "         <font size=+4 color=0043CF><b>$counter</b></font>";
            else {
               print "           <A HREF=\"search.php?searchfor=".rawurlencode($this->searchstring)."&hits=$this->hits&start=$i\">";
               $string = "$counter";
               for ($q = 0; $q < strlen($string); $q++) {
                  print "<IMG SRC=\"pix/$string[$q]_$numstyl.jpg\" border=0 ALT=\"$counter\">";
               } 
               print "</a>\n";
            }
            print "&nbsp;</TD>\n";
         }
         if ($this->totalhits > ($this->hits + $this->start)) {
         print "           <TD><A HREF=\"search.php?searchfor=".rawurlencode($this->searchstring)."&start=".($this->start + $this->hits)."&hits=$this->hits\" ALT=\"Next\">Next--></A></TD>\n";
         } else print "           <TD><FONT color=FFFFFF>Next--></FONT></TD>\n";
      } print "         </TR>\n       </TABLE>";
      $this->displaysearch();
   } // end function display

} // end class 

} // end include protection

?>
