# phpMyAdmin SQL Dump
# version 2.5.4
# http://www.phpmyadmin.net
#
# Host: localhost
# Generation Time: Feb 26, 2005 at 11:26 AM
# Server version: 4.0.23
# PHP Version: 4.3.10-2
# 
# Database : `IndeXor`
# 

# --------------------------------------------------------

#
# Table structure for table `files`
#

CREATE TABLE `files` (
  `id` mediumint(9) NOT NULL auto_increment,
  `host_id` mediumint(9) NOT NULL default '0',
  `share_id` mediumint(9) NOT NULL default '0',
  `file_path` char(128) NOT NULL default '',
  `file_name` char(200) NOT NULL default '',
  `size` double NOT NULL default '0',
  `extention` char(5) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`),
  KEY `share_id` (`share_id`),
  KEY `host_id` (`host_id`),
  KEY `file_path` (`file_path`),
  KEY `file_name` (`file_name`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

# --------------------------------------------------------

#
# Table structure for table `hosts`
#

CREATE TABLE `hosts` (
  `id` mediumint(9) NOT NULL auto_increment,
  `name` char(40) NOT NULL default '',
  `ip` char(15) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`),
  KEY `name` (`name`),
  KEY `ip` (`ip`)
) TYPE=MyISAM AUTO_INCREMENT=19 ;

# --------------------------------------------------------

#
# Table structure for table `keyword`
#

CREATE TABLE `keyword` (
  `file_id` mediumint(9) NOT NULL default '0',
  `word` char(84) NOT NULL default '',
  KEY `word` (`word`),
  KEY `file_id` (`file_id`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `log`
#

CREATE TABLE `log` (
  `id` mediumint(9) NOT NULL auto_increment,
  `searchfor` varchar(64) NOT NULL default '',
  `results` mediumint(9) NOT NULL default '0',
  `ip` varchar(15) NOT NULL default '',
  `hostname` tinytext NOT NULL,
  `time` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `ip` (`ip`),
  KEY `searchfor` (`searchfor`),
  KEY `id` (`id`)
) TYPE=MyISAM AUTO_INCREMENT=4 ;

# --------------------------------------------------------

#
# Table structure for table `shares`
#

CREATE TABLE `shares` (
  `id` mediumint(9) NOT NULL auto_increment,
  `name` char(40) NOT NULL default '',
  `host_id` mediumint(9) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`),
  KEY `name` (`name`),
  KEY `host_id` (`host_id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

# --------------------------------------------------------

#
# Table structure for table `skipwords`
#

CREATE TABLE `skipwords` (
  `word` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`word`),
  KEY `word` (`word`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `words`
#

CREATE TABLE `words` (
  `id` mediumint(9) NOT NULL auto_increment,
  `word` varchar(128) NOT NULL default '',
  PRIMARY KEY  (`id`,`word`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;
