<?php

include("func_indexor_tbl_log.php");
include("func_indexor_tbl_hosts.php");
include("func_extra.php");
include("config.php");
include("func_indexorsearch.php");


if($searchfor) {

	$log = new IndeXorLog($debug);
	$so = new IndeXorSearch($debug);
	$so->search($searchfor, $start, $hits);
	$log->store($searchfor, $so->totalhits, $REMOTE_ADDR);
	printheader("IndeXor Search Results for $searchfor");
	print "<CENTER>\n";
	$so->display($layout);
	print "</CENTER>\n\n";
	printfooter("short");

} else {
	
	$host = new HOSTS($DEBUG);

	printheader("IndeXor Search Results for: $searchfor");
	include("search.html");
	printfooter("short");

}

?>
