<?php

global $FUNC_INDEXOR_TBL_SKIPWORDS_INC;
if (!$FUNC_INDEXOR_TBL_SKIPWORDS_INC) {
   $FUNC_INDEXOR_TBL_SKIPWORDS_INC=1;

   include("func_db_mysql.php");

   class SkipWords {

      var $db, $resultid;
      var $table, $debug;

      function SkipWords($debug) {
         global $INDEXOR_DBUSER, $INDEXOR_DBNAME, $INDEXOR_DBHOST, $INDEXOR_DBPASSWD;
			$this->debug = $debug;
         $this->table = "Skipwords";
         $this->db = new Database($debug);
         $this->db->connect($INDEXOR_DBHOST, $INDEXOR_DBUSER, $INDEXOR_DBPASSWD);
         $this->db->usedatabase($INDEXOR_DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where Id=\"$id\""));
		}

		function chop_words($array) {

			for($i = 0; $i < count($array); $i++) {
				if($this->exists($array[$i])) {
					if($this->debug) print "This is a skip $array[$i]<BR>\n";
				} else {
					if($this->debug) print "This is not a skip word: $array[$i]<BR>\n";
					$ret[] = $array[$i];
				}
			}
			return $ret;

		}

		function exists($word) {
			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where Word=\"$word\""));
			return $array[count];
		}
	
	} // end class SKIPWORDS

} // end include protection

?>
