<?php

	include("../config.inc");
	include($INC[message_board]);
	include($INC[logs]);
	include($INC[extra]);
	$log = new LOGS($debug);
	$log->update($REMOTE_ADDR, $HTTP_USER_AGENT);

	$message = new message_board($DEBUG);

	printheader();
	include($HTML[message_board_stats]);
	printfooter("banner");

?>
