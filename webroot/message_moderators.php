<?php

	include("../config.inc");

	include($INC[moderators]);
	include($INC[moderated]);
	include($INC[message_board]);

	if(session_is_registered("auth") && session_is_registered("modarray") && $auth == 1) {

		$mod = new Moderators($DEBUG);
		$moded = new Moderated($DEBUG);
		$mes = new Message_Board($DEBUG);
	
		if($modarray[id] && $array[id] && $moder == 1) {
			if($moded->exists($modarray[id], $array[id])) {
				$msg .= "<font color=ff0000>You have already DROPPED that post.</font><BR>\n";
			} else {
				$moded->insert_mod($modarray[id], $array[id], $modarray[modvalue]);
				$mes->update_modvalue($array[id], $modarray[modvalue]);
			}
		} else if($modarray[id] && $array[id] && $moder == 0) {
			if($moded->exists($modarray[id], $array[id])) {
				$msg .= "<font color=ff0000>You have already DROPPED that post.</font><BR>\n";
			} else {
				$moded->insert_mod($modarray[id], $array[id], -$modarray[modvalue]);
				$mes->update_modvalue($array[id], -$modarray[modvalue]);
			}
		}

		printheader();
		include($HTML[message_moderators]);
		printfooter();

	} else {
		header("Location: ".$CONTENT[pages][message_board]);
	}


?>
