<?php

	include("../config.inc");
	include($INC[logs]);
	include($INC[users]);
	
	$log = new LOGS($debug);
	$log->update($REMOTE_ADDR, $HTTP_USER_AGENT);
	$user = new Users($DEBUG);

	if(session_is_registered("auth") && $auth == 1) {
		if($Save) {
			$array[email] = strip_tags($array[email]);
			$array[telephone] = strip_tags($array[telephone]);
			$array[sig] = strip_tags($array[sig]);
			$array[sig] = str_replace("\r\n", "\n", $array[sig]);
			$array[sig] = str_replace("\n", "<BR>\n", $array[sig]);
			$user->SaveInfo($array);
			header("Location: $array[ref]");
		} else {
			$array = $user->get_info_username($username);
			$array[sig] = str_replace("<BR>\n", "\n", $array[sig]);
			printheader();
			include($HTML[$CONTENT[name][basename($PHP_SELF)]]);
			printfooter("banner");
		}
	} else {
		header("Location: $PAGES[login]");
	}

?>
