<?php

include("../config.inc");
include($INC[logs]);
include($INC[Pictures]);

$log = new LOGS($debug);
$log->update($REMOTE_ADDR, $HTTP_USER_AGENT);

$pic = new Pictures($debug);

printheader();
include($HTML[my_pictures]);

if($button && $file1) {
	$pic->insert_pic($userid, file_get_contents($_FILES[file1][tmp_name]));
	print "Thank you for uploading ".$_FILES[file1][name]." this will showup on the web site as soon as the picture is approved.<BR>\n";
}

printfooter("banner");

?>
