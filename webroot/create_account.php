<?php

	include("../config.inc");

	include($INC[logs]);
	include($INC[users]);
	
	$log = new LOGS($debug);
	$log->update($REMOTE_ADDR, $HTTP_USER_AGENT);

	if(!$HTTP_REFERER) {
		header("Location: ".$CONTENT[pages][login]);
		exit;
	}

	if($button && $array[username] && $array[password1] && $array[password2]) {

		$user = new Users($debug);
		$array[ip] = $REMOTE_ADDR;

		if($array[password1] != $array[password2]) {
			$msg .= "Your passwords do not match.";
		} else {
			$array[password] = $array[password1];
		}
		$array[password1] = $array[password2] = '';

		if($user->user_exists($username)) {
			$msg .= "The username you have selected already exists please try something else.";
		}
		if(!$msg) {
			session_destroy();
			printheader(0, 5);
			print "<center><BR><BR>";
			$user->create_user($array);
			printfooter("short");
		} else {
			printheader();
			include($HTML[$CONTENT[name][basename($PHP_SELF)]]);
			printfooter("short");
		}
	} else {
		printheader();
		include($HTML[$CONTENT[name][basename($PHP_SELF)]]);
		printfooter("short");
	}
?>
