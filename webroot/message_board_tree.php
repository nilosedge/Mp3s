<?php

	include("../config.inc");
	include($INC[message_board]);
	include($INC[logs]);
	include($INC[extra]);
	$log = new LOGS($debug);
	$log->update($REMOTE_ADDR, $HTTP_USER_AGENT);

	$message = new message_board($DEBUG);

	printheader(3);
	include($CONTENT[html][message_board_tree]);
	printfooter("banner");

?>
