<?php 

	include("../config.inc");
	include($INC[message_board]);
	include($INC[logs]);
	include($INC[extra]);
	include($INC[users]);
	
	$log = new LOGS($DEBUG);
	$log->update($REMOTE_ADDR, $HTTP_USER_AGENT);

	$message = new message_board($DEBUG);
	$array = $message->get_info($id);

	$user = new USERS($DEBUG);
	$ar = $user->get_info_username($array[name]);
	$sig = $ar[sig];

	printheader();
	include("$HTML[message_view_thread]");
	printfooter("banner");

?>
