<?php

	include("../config.inc");
	include($INC[message_board]);

	if($searchfor) {
		$message = new message_board($DEBUG);
		printheader();
		$message->search($searchfor, $searchby);
		include($HTML[message_board_search]);
		printfooter("banner");
	} else {
		header("Location: ".$CONTENT[pages][message_board]);
	}

?>
