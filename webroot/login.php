<?php
include("../config.inc");
include($INC[extra]);
include($INC[logs]);
include($INC[users]);
include($INC[moderators]);
$log = new LOGS($debug);
$log->update($REMOTE_ADDR, $HTTP_USER_AGENT);


if($HTTP_COOKIE_VARS["username"] && $HTTP_COOKIE_VARS["password"]) {
	$username = $HTTP_COOKIE_VARS["username"];
	$password = $HTTP_COOKIE_VARS["password"];
	$button = 1;
	$refer = $CONTENT[pages][index];
}
session_destroy();

if($button) {
	$user = new USERS($debug);
	$mod = new Moderators($debug);
	if($auth = $user->auth($username, $password)) {	
		$array = $user->get_info_username($username);
		$userinfo = $array;
		$modarray = $mod->get_info_userid($array[id]);
		$userid = $array[id];
		$email = $array[email];
		if($username == "-=Nilo=-") { $admin = 1; session_register("admin"); }
		if($username == "lucina") { $admin = 1; session_register("admin"); }
		if($modarray) session_register("modarray");
		session_register("auth");
		session_register("userinfo");
		session_register("username");
		session_register("userid");
		session_register("email");

		setcookie("username", $username, ((time() + 3600) * 24 * 5), "/", "mp3s.nilosplace.net");
		setcookie("password", $password, ((time() + 3600) * 24 * 5), "/", "mp3s.nilosplace.net");

		if(basename($refer) == basename($PHP_SELF)) $refer = $CONTENT[pages][index];

		if($download) header("Location: ".$CONTENT[pages][result]."?download=$download"); 
		else if($random) header("Location: ".$CONTENT[pages][result]."?random=$random"); 
		else if($classified) header("Location: ".$CONTENT[pages][classifieds]);
		else header("Location: ".$refer); 
	} else {
		$msg = "Incorrect username and or password, please try again. If you have forgotten your password click the link below to have your password emailed to you.";
		printheader();
		include($HTML[login]);
		printfooter("banner");
	}

}
else {
	printheader();
	include($HTML[login]);
	printfooter("banner");
}

?>
