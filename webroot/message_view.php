<?php

	include("../config.inc");
	include($INC[message_board]);
	include($INC[logs]);
	include($INC[extra]);
	include($INC[users]);
	
	$log = new LOGS($debug);
	$log->update($REMOTE_ADDR, $HTTP_USER_AGENT);

	$message = new message_board($DEBUG);
	$user = new Users($DEBUG);

	printheader();

	$array = $message->get_info($array[id]);
	$ar = $user->get_info_username($array[name]);
	$sig = $ar[sig];

	include("$HTML[message_view]");

	printfooter("short");

?>
