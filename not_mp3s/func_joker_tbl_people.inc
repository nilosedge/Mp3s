<?php

global $FUNC_JOKER_INC;
if (!$FUNC_JOKER_INC){

$FUNC_JOKER_INC=1;

include($INC[db]);

class JOKER {

   var $db, $resultid, $table; 
   var $searchstring, $totalhits;
   var $hits, $start, $debug;

   function JOKER($debug){
      global $DBUSER, $DBHOST, $DBPASSWD, $DBJOKER;
		$this->debug = $debug;
      $this->table = "people";
      $this->db = new Database($debug);
      $this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
      $this->db->usedatabase($DBJOKER);

   }

   function randomsearch($sex, $searchtype) {
			# $sex = strtolower($sex);
         if (!($searchtype)) {
            $searchtype = "random";
         }   
         if ($searchtype == "random") {
            $query = "select id from $this->table where sex=\"$sex\"";
         } elseif ($searchtype == "vespers") {
            $query = "select id from $this->table where sex=\"$sex\" and soc_stat = \"sg\"";
         } else {
            if ($this->debug) print "Unknown search type<BR>\n";
         }
         $resultid = $this->db->query($query);
         $rows = $this->db->num_rows($resultid);
         $random = rand(1, $rows);
         $counter = 0;
         while ($temp = $this->db->get_array($resultid)) {
            $woarray[$counter] = $temp[0];
            $counter++;
         }
         return $this->getinfo($woarray[$random]);
   }
   function getinfo($pid) {
         $array = $this->db->get_array($this->db->query("select * from $this->table where id=\"$pid\""));
			$array1 = $this->db->get_array($this->db->query("select display_value from academy where code = \"$array[academy]\""));
			$array[academy] = $array1[display_value];
			$array1 = $this->db->get_array($this->db->query("select display_value from soc_stat where code = \"$array[soc_stat]\""));
			$array[soc_stat] = $array1[display_value];
			$array1 = $this->db->get_array($this->db->query("select display_value from class_standing where code = \"$array[class_standing]\""));
			$array[class_standing] = $array1[display_value];
			$array1 = $this->db->get_array($this->db->query("select code from major_codes where id = \"$array[id]\""));
			$array[major] = $array1[code];
			$array1 = $this->db->get_array($this->db->query("select email from email where id = \"$array[id]\""));
			$array[email] = $array1[email];
			$array1 = $this->db->get_array($this->db->query("select display_value,department from major_translation where code = \"$array[major]\""));
			$array[major] = $array1[display_value];
			$array[dept] = $array1[department];

			switch($array[residence]) {
				case("v"):
					$array[residence] = "Village";
					break;
				case("h"):
					$array[residence] = "Student Housing";
					break;
				case("m"):
					if(!$array[phone]) { $array[residence] = "Talge Hall"; break; }
					if(($array[phone] - 3000) < 400 && ($array[phone] - 3000) > 75) 
						$array[residence] = "Talge Hall Room (".($array[phone] - 3000).")";
					else $array[residence] = "Conference Center Somewhere";
					break;
				case("w"):
					if(!$array[phone]) { $array[residence] = "Thather Hall"; break; }
					if(($array[phone] - 2000) < 400 && strlen($array[phone]) == 4) 
						$array[residence] = "Thather Hall Room (".($array[phone] - 2000).")";
					else $array[residence] = "Conference Center Somewhere";
					break;
				default:
					$array[residence] = "Unknown";
					break;
			}

         return $array;
   }

   function search($searchstring, $start, $hits) {


      $this->searchstring = $searchstring;
      if (!($start)) {
      $start = 0;
      }
      if (!($hits)) {
         $hits = 10;
      }
      $this->start = $start;
      $this->hits = $hits;

      # $this->searchstring = $searchfor;
         $searchquery = "select * from $this->table where 
                      p_name like '%$this->searchstring%' or 
                      m_name like '%$this->searchstring%' or 
                      l_name like '%$this->searchstring%' or 
                      f_name like '%$this->searchstring%' or 
                      message like '%$this->searchstring%' or
                      phone like '%$this->searchstring%' or
                      residence like '%$this->searchstring%'
                      order by p_name";
      # print "Our search query is $searchquery<BR>\n";
      $this->resultid = $this->db->query($searchquery);
      $this->totalhits = $this->db->num_rows($this->resultid);
      print "Total number of hits is $this->totalhits<BR>\n";
      $tempquery = $searchquery." limit $this->start, $this->hits";
      $this->resultid = $this->db->query($tempquery);
      return $this->resultid;

   }

   function advsearch() {

   }

   function display() {
			global $auth;
         $numresult = $this->db->num_rows($this->resultid);
         print "<!-- Starting Table Generation -->\n";
         if ($numresult) {
            if (($this->start + $this->hits) > ($this->start + $numresult)) {
               $temp = $this->start + $numresult;
            }
            else {
               $temp = $this->start + $this->hits;
            }
            print "Image results ".($this->start + 1)." - ".($temp)." of $this->totalhits total Matches.<BR><BR>\n";
         print "<TABLE border=0 cellpadding=0 cellspacing=0>";   
            # for ($step = 0; $step < $numresult; $step++) {
             while (  $res1 = $this->db->get_array($this->resultid)) {
				$res1 = $this->getinfo($res1[id]);
            if (!($res1[email])) {
               $res1[email] = "&nbsp;";
            }
            if (!($res1[message])) {
               $res1[message] = "&nbsp;";
            }
      if ($auth) {
          print "
         <TR>
           <TD align=left><A HREF=\"javascript:datebox('$res1[id]','$res1[sex]')\" onMouseOut=\"statusbox(''); return true;\" onMouseOver=\"statusbox('$res1[p_name]'); return true;\" ><IMG width=150 heigth=192 border=0 alt=\"$res1[p_name]\" SRC=\"pictures/$res1[picture]\"></A></TD>
           <TD align=center>
               <TABLE width=100% border=1>
                  <TR><TD align=center rowspan=5>
                     <TABLE border=0>
                        <TR><TD align=left>Name:</TD></TR>
                        <TR><TD align=left>Local Phone:</TR>
                        <TR><TD align=left>Residence:</TD></TR>
                        <TR><TD align=left>Social Status:</TD></TR>
                        <TR><TD align=left>Comment:</TD></TR>
                     </TABLE>
                  </TD><TD align=center>
                     <TABLE border=0>
                        <TR><TD>$res1[p_name]</TD></TR>
                        <TR><TD>$res1[phone]</TD></TR>
                        <TR><TD>$res1[residence]</TD></TR>
                        <TR><TD>$res1[soc_stat]</TD></TR>
                        <TR><TD>$res1[message]</TD></TR>
                     </TABLE>
                  </TD></TR>
               </TABLE>
            </TD>
         </TR>\n";
      }
      else {
      print "
         <TR>
           <TD>
               <TABLE border=0 width=70%>
                  <TR><TD><A HREF=\"pictures/$res1[picture]\" alt=\"$res1[p_name]\"><IMG width=75 heigth=96 border=0 alt=\"$res1[p_name]\" SRC=\"pictures/$res1[picture]\"></A><BR><BR></TD>
                     <TD>
                        <TABLE border=1>
                           <TR><TD align=left>Name:</TD><TD>$res1[p_name]</TD></TR>
                           <!--<TR><TD align=left>Email:</TD><TD>$res1[email]</TD></TR>-->
                           <TR><TD align=left>Comment:</TD><TD>$res1[message]</TD></TR>
                        </TABLE>
                     </TD>
                  </TR>
               </TABLE>
            </TD>
         </TR>\n";
      }
      # $this->displaynumbers();
      }
      print "       </TABLE>\n";
      }
      else {
         print "Sorry was unable to find anything that matched your request<BR>\n";
      }
      print "<!-- Ending Table Generation -->\n";




   } 

   function displaynumbers() {
   global $PHP_SELF;
   print "<BR><BR><CENTER>";
   print "<TABLE><TR>";
   print "<TD><A HREF=\"pix/0.jpg\"><IMG SRC=\"pix/0.jpg\" border=0 ALT=\"0\"></A></TD>";
   print "<TD><A HREF=\"pix/1.jpg\"><IMG SRC=\"pix/1.jpg\" border=0 ALT=\"1\"></A></TD>";
   print "<TD><A HREF=\"pix/2.jpg\"><IMG SRC=\"pix/2.jpg\" border=0 ALT=\"2\"></A></TD>";
   print "<TD><A HREF=\"pix/3.jpg\"><IMG SRC=\"pix/3.jpg\" border=0 ALT=\"3\"></A></TD>";
   print "<TD><A HREF=\"pix/4.jpg\"><IMG SRC=\"pix/4.jpg\" border=0 ALT=\"4\"></A></TD>";
   print "<TD><A HREF=\"pix/5.jpg\"><IMG SRC=\"pix/5.jpg\" border=0 ALT=\"5\"></A></TD>";
   print "<TD><A HREF=\"pix/6.jpg\"><IMG SRC=\"pix/6.jpg\" border=0 ALT=\"6\"></A></TD>";
   print "<TD><A HREF=\"pix/7.jpg\"><IMG SRC=\"pix/7.jpg\" border=0 ALT=\"7\"></A></TD>";
   print "<TD><A HREF=\"pix/8.jpg\"><IMG SRC=\"pix/8.jpg\" border=0 ALT=\"8\"></A></TD>";
   print "<TD><A HREF=\"pix/9.jpg\"><IMG SRC=\"pix/9.jpg\" border=0 ALT=\"9\"></A></TD>";
   print "</TR></TABLE>";

   print "
       <FORM method=GET action=\"$PHP_SELF\">
         <FONT color=\"000000\"><INPUT type=text name=searchfor size=31 maxlength=256 value=\"$this->searchstring\"> <INPUT name=search type=submit value=\"Mp3 Search\"></FONT>
         
       </FORM>
";




  }

}

}
