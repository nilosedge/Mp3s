<?php

global $FUNC_MP3S_TBL_CLASSIFIEDS_INC;
if (!$FUNC_MP3S_TBL_CLASSIFIEDS_INC) {
   $FUNC_MP3S_TBL_CLASSIFIEDS_INC=1;

   include($INC[db]);

   class CLASSIFIEDS {

      var $db, $resultid;
      var $table, $debug;

      function CLASSIFIEDS($debug) {
         global $DBUSER, $DBNAME, $DBHOST, $DBPASSWD;

			$this->debug = $debug;
         $this->table = "Classifieds";
         $this->db = new Database($debug);
         $this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
         $this->db->usedatabase($DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=$id"));
		}

		function InsertClass($info) {
			global $username;
			$this->db->query("insert into $this->table (username, SmallDescription, LargeDescription, WebSite, Price, CategoryId, Created, Modified) values(\"$username\", \"$info[SmallDescription]\", \"$info[LargeDescription]\", \"$info[WebSite]\", \"$info[Price]\", $info[CategoryId], now(), now())");
		}

		function UpdateCounter($id) {
			$this->db->query("update $this->table set hits=hits+1 where id=$id");
		}

		function DeleteClass($username, $id) {
			$this->db->query("delete from $this->table where username=\"$username\" and id=$id");
			return $this->get_info($id);
		}

		function ModifyClass($username, $info) {
			$this->db->query("update $this->table set SmallDescription=\"$info[SmallDescription]\", LargeDescription=\"$info[LargeDescription]\", WebSite=\"$info[WebSite]\", Picture=\"$info[Picture]\", Price=\"$info[Price]\", CategoryId=$info[CategoryId], Modified=now() where username=\"$username\" and id=$info[id]");
		}

		function PrintLast($num = 100) {
			global $CONTENT;
			$res = $this->db->query("select Classifieds.*, users.*, Classifieds.id as cid from $this->table, users where $this->table.Username=users.username and Modified > (curdate() - INTERVAL 1 MONTH) order by Modified limit 0, $num");
			print "<table width=100%><tr><td>Description</td><td>User</td><td>Price</td><td>Telephone</td></tr>";
			while($array = $this->db->get_array($res)) {
				print "<tr><td><a href=\"".$CONTENT[pages][classified_view]."?array[id]=$array[cid]\">$array[SmallDescription]</a></td><td><a href=\"mailto:$array[email]\">$array[Username]</a></td><td>$array[Price]</td><td>$array[telephone]</td></tr>";
			}
			print "</table>";
		}

		function PrintUsers($username) {
			global $CONTENT;
			$res = $this->db->query("select $this->table.*, Categories.Name, (modified + INTERVAL 1 MONTH) as Expires from $this->table, Categories where username=\"$username\" and $this->table.CategoryId=Categories.id");

			if($this->db->num_rows($res)) {

				print "
						<table width=100% border=1>
							<tr>
								<td>Actions</td>
								<td>Small Description</td>
								<td>Price</td>
								<td>Category</td>
								<td>Hits</td>
								<td>Created</td>
								<td>Expires</td>
							</tr>\n";
						
				while($array = $this->db->get_array($res)) {
	
					print "
							<tr>
								<td><a href=\"".$CONTENT[pages][classified_view]."?array[id]=$array[id]\">View</a><a href=\"".$CONTENT[pages][classified_new]."?edit_button=1&array[id]=$array[id]\">Edit</a> <a href=\"".$CONTENT[pages][classified_new]."?delete_button=1&array[id]=$array[id]\">Delete</a></td>
								<td>$array[SmallDescription]</td>
								<td>$array[Price]</td>
								<td>$array[Name]</td>
								<td>$array[Hits]</td>
								<td>".Date("M d, Y, H:i a", convert_mysql_time($array[Created]))."</td>
								<td>".Date("M d, Y, H:i a", convert_mysql_time($array[Expires]))."</td>
							</tr>\n";
				}
				print "</table>";
			} else {
				print "You have not setup anything to sell.";
			}
		} // end function PrintUser()
		
	} // end class CLASSIFIEDS

} // end include protection

?>
