<?php

global $FUNC_MP3S_TBL_CATEGORIES_INC;
if (!$FUNC_MP3S_TBL_CATEGORIES_INC) {
   $FUNC_MP3S_TBL_CATEGORIES_INC=1;

   include($INC[db]);

   class CATEGORIES {

      var $db, $resultid;
      var $table, $debug;

      function CATEGORIES($debug) {
         global $DBUSER, $DBNAME, $DBHOST, $DBPASSWD;

			$this->debug = $debug;
         $this->table = "Categories";
         $this->db = new Database($debug);
         $this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
         $this->db->usedatabase($DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=\"$id\""));
		}

		function PrintSelected($id=0) {
			$selected[$id] = " SELECTED";
			$res = $this->db->query("select * from $this->table");
			print "<select name=array[CategoryId]>\n";
			print "<option value=\"0\"".$selected[0].">Select Category</option>\n";
			while($array = $this->db->get_array($res)) {
				print "\t<option value=\"$array[id]\"".$selected[$array[id]].">$array[Name]</option>\n";
			}
			print "</select>\n";
		}
	
	} // end class CATEGORIES

} // end include protection

?>
