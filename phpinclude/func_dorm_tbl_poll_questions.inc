<?php

global $FUNC_DORM_TBL_POLL_QUESTIONS_INC;
if (!$FUNC_DORM_TBL_POLL_QUESTIONS_INC) {
   $FUNC_DORM_TBL_POLL_QUESTIONS_INC=1;

   include($INC[db]);

   class POLL_QUESTIONS {

      var $db, $resultid;
      var $table, $debug;

      function POLL_QUESTIONS($debug) {
         global $DORM_DBUSER, $DORM_DBNAME, $DORM_DBHOST, $DORM_DBPASSWD;

			$this->debug = $debug;
         $this->table = "poll_questions";
         $this->db = new Database($debug);
         $this->db->connect($DORM_DBHOST, $DORM_DBUSER, $DORM_DBPASSWD);
         $this->db->usedatabase($DORM_DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=\"$id\""));
		}

		function get_latest() {
			return $this->db->get_array($this->db->query("select * from $this->table order by time desc"));
		}

		function print_last($num) {
			global $PHP_SELF;
			$result = $this->db->query("select * from $this->table order by time desc");
			print "<table width=80% border=1>";
			print "<tr><td>Poll</td><td>Date Posted.</td></tr>\n";
			while($array = $this->db->get_array($result)) {
				print "<tr><td><a href=\"$PHP_SELF?view=polls&qid=$array[id]\">$array[question]</a></td><td>".date("F j, Y",  convert_mysql_time($array[time]))."</td></tr>\n";
			}
			print "</table>\n";
		}

		function insert($question) {
			$this->db->query("insert into $this->table (question) values(\"$question\")");
			//print("insert into $this->table (question) values(\"$question\")");
			$ar = $this->db->get_array($this->db->query("select id from $this->table where question=\"$question\""));
			//print("select id from $this->table where question=\"$question\"");
			//return 0;
			return $ar[id];
		}
	
	} // end class POLL_QUESTIONS

} // end include protection

?>
