<?php

	global $PARSE_TOKEN_INC;
	if(!$PARSE_TOKEN_INC) {
		$PARSE_TOKEN_INC = 1;

		/*
			<string> -> <quoted>{ <quoted>}
			<quoted> -> <token>|"<string>"
			<token>  -> /w{/w}
		*/

		class PARSE_TOKEN {
			var $s, $a, $debug;

			function PARSE_TOKEN($string, $debug = 0) {
				$this->debug = $debug;
				$this->s = $string;
			}
			function set_string($string) {
				$this->s = $string;
			}
			function get_array() {
				$this->a = $this->parse($this->s);
				# for($i = 0; $i < count($this->a); $i++) {
					# print "Array[$i]: ".$this->a[$i]."<BR>\n";
				#}
				return $this->a;
			}
			function parse($string) {
				# print "This is the string that we are going to parse $string<BR>\n";
				$string = str_replace(",", " ", $string);
				$string = str_replace(".", " ", $string);
				$string = str_replace("-", " ", $string);
				$string = stripslashes($string);
				$string = trim($string);

				for($i = 0; $i < strlen($string); $i++) {
					$str[] = $string[$i];
				}
				$array = $this->parse_string($str);
				for($i = 0; $i < count($array); $i++) {
					if($this->debug) print "This is the word $i $array[$i]<BR>\n";
					$arr[$array[$i]] = 1;
				}
				foreach ($arr as $key => $value) {
					if($this->debug) echo "Key: $key; Value: $value<br>\n";
					$ret[] = $key;
				}
				return $ret;
			}
			function parse_string(&$str) {
				$this->print_string($str, "string");
				while(count($str)) {
					while($str[0] == " ") array_shift($str);
					if($str[0] == "\"") {
						array_shift($str);
						while($str[0] && $str[0] != "\"") {
							if($str[0] == "'") $quote .= array_shift($str);
							//elseif($quote) $quote .= " ";
							# print "We are in the loop<BR>\n";
							$len = count($str);
							$quote .= $this->parse_tok($str);
							if(count($str) == $len) { $return[] = $str; return $return; }
						}
						# print "This is the val of quote: $quote<BR>\n";
						if($str[0] == "\"") array_shift($str);
						$return[] = $quote;
						$quote = "";
					} else if($str[0] == "'") {
						array_shift($str);
						while($str[0] && $str[0] != "'") {
							if($str[0] == "\"") $quote .= array_shift($str);
							//elseif($quote) $quote .= " ";
							# print "We are in the loop<BR>\n";
							$len = count($str);
							$quote .= $this->parse_tok($str);
							if(count($str) == $len) { $return[] = $str; return $return; }
						}
						# print "This is the val of quote: $quote<BR>\n";
						if($str[0] == "'") array_shift($str);
						$return[] = $quote;
						$quote = "";
					} else {
						$len = count($str);
						$return[] = $this->parse_tok($str);
						if(count($str) == $len) { $return[] = $str; return $return; }
					}
				}
				return $return;
			}

			function parse_tok(&$str) {
				if($this->debug) $this->print_string($str, "tok");
				while($str[0] == " ") array_shift($str);
				while(ereg("[-\.%0-9a-zA-Z()]", $str[0])) {
					$token .= array_shift($str);
				}
				if($this->debug) print "This is the return token $token<BR>\n";
				return $token;
			}

			function print_string(&$str, $fun) {
				if($this->debug) print "This is whats left in $fun ---->";
				for($i = 0; $i < count($str); $i++) {
					if($this->debug) print $str[$i];
				}
				if($this->debug) print "<------<BR>\n";
			}

		} // end class PARSE_TOKEN
	} // end include protection

