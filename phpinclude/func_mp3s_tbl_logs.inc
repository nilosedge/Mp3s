<?php

global $FUNC_LOGS_INC;
if (!$FUNC_LOGS_INC){
	$FUNC_LOGS_INC=1;

	include($INC[db]);

	class LOGS {

	   var $db, $resultid, $table, $debug;

	   function LOGS($debug){
			global $DBUSER, $DBNAME, $DBHOST, $DBPASSWD;
	      $this->db = new Database($debug);
	      $this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
	      $this->db->usedatabase($DBNAME);
			$this->table = "logs";
			$this->debug = $debug;
	   }

		function update($ip, $browserinfo) {
			$array = $this->get_ip($ip);
			if (!$array) {
				$this->db->query("insert into $this->table (ip, count, browserinfo) values('$ip',1,'$browserinfo')");
			}
			else {
				$array[count]++;
				$this->db->query("update $this->table set browserinfo='$browserinfo', count='$array[count]' where ip='$ip'");
			}
		} // end function update

		function get_ip($ip) {
			return $this->db->get_array($this->db->query("select * from $this->table where ip='$ip'"));
		} // end function get_ip

		function get_stats() {
			return $this->db->get_array($this->db->query("select sum(count) as sum, count(*) as count from $this->table"));
		} // end function get_stats

	   function display(){
			if (!$this->resultid) {
				$veiwlogs = "select * from $this->table";
				$this->resultid = $this->db->query($veiwlogs);
			}
	      $numresult = $this->db->num_rows($this->resultid);

			for($step = 0; $step < $numresult; $step++){
            if( !$step ){
   		      print "
                  <TABLE border=1>
                     <TR ALIGN=CENTER>
                        <TD><U>IP</U></TD>
                        <TD><U>Browser</U></TD>";
      		}
            $res2 = $this->db->get_array($this->resultid);
            print "
               <TR>
                  <TD>$res2[ip]</TD>
                  <TD>$res2[browserinfo]</TD>
               </TR>";
			}
      	print "\n</TABLE><BR><BR>\n";
		} // end function display()
	} // end class LOGS
} // end include protection
