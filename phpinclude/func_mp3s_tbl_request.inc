<?php

global $FUNC_REQUEST_INC;
if (!$FUNC_REQUEST_INC){

   $FUNC_REQUEST_INC=1;

	include($INC[db]);

   class REQUEST {

      var $db, $resultid, $table, $debug;

      function REQUEST($debug=0){
         global $DBUSER, $DBNAME, $DBHOST, $DBPASSWD;
         $this->db = new Database($debug);
         $this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
         $this->db->usedatabase($DBNAME);
         $this->table = "requests";
         $this->debug = $debug;
      }

      function insert($request) {
         global $REMOTE_ADDR;
         $this->db->query("insert into $this->table (ip, request, time_requested) values('$REMOTE_ADDR', '$request', ".time().")");
      }

      function found($ip, $filepath) {
         if($filepath) $this->db->query("update $this->table set found=1, filepath=\"$filepath\" where ip=\"$ip\"");
         else if($this->debug) print "You did not give a filepath to insert<BR>\n";
      }

      function display($layout){
         if($layout == "found") $veiwlogs = "select * from $this->table where found=1";
         elseif($layout == "notfound") $veiwlogs = "select * from $this->table where found=0";

         $this->resultid = $this->db->query($veiwlogs);
         $numresult = $this->db->num_rows($this->resultid);
         print "<UL>";
         for($step = 0; $step < $numresult; $step++){
            $res2 = $this->db->get_array($this->resultid);
            print "<LI>$res2[request]<BR>(Requested ".date("m/d/y h:m:s", $res2[time_requested]).")<BR><BR>";
         }
         print "</UL>";
      } // end function display()
   } // end class REQUEST 
} // end include protection
