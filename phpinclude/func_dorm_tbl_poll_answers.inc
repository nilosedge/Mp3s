<?php

global $FUNC_DORM_TBL_POLL_ANSWERS_INC;
if (!$FUNC_DORM_TBL_POLL_ANSWERS_INC) {
   $FUNC_DORM_TBL_POLL_ANSWERS_INC=1;

   include($INC[db]);

   class POLL_ANSWERS {

      var $db, $resultid;
      var $table, $debug;

      function POLL_ANSWERS($debug) {
         global $DORM_DBUSER, $DORM_DBNAME, $DORM_DBHOST, $DORM_DBPASSWD;

			$this->debug = $debug;
         $this->table = "poll_answers";
         $this->db = new Database($debug);
         $this->db->connect($DORM_DBHOST, $DORM_DBUSER, $DORM_DBPASSWD);
         $this->db->usedatabase($DORM_DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=\"$id\""));
		}

		function print_options($qid) {
			$resultid = $this->db->query("select * from $this->table where qid=\"$qid\"");

			if($this->db->num_rows($resultid)) {
				print "<BR><table>\n";

				while($array = $this->db->get_array($resultid)) {
					print "<tr><td><input type=radio name=answer value=\"$array[id]\">$array[answer]</td></tr>\n";
				}
				print "<input type=hidden name=qid value=$qid>";
				print "</table>\n";
			}
		}

		function print_results($qid) {
			$arr = $this->db->get_array($this->db->query("select count(qid) as count from poll_votes where qid=$qid"));
			$votes = $arr[count];
			#print "Total number was $votes<BR>\n";
			$resultid = $this->db->query("select * from $this->table where qid=$qid");
			$qa = $this->db->get_array($this->db->query("select * from poll_questions where id = $qid"));

			print "<h1>$qa[question]</h1><BR><FONT color=000000><table bgcolor=FFFFFF width=550 border=0 cellpadding=2 cellspacing=2>";

			while($array = $this->db->get_array($resultid)) {
				$array2 = $this->db->get_array($this->db->query("select count(*) as votes from poll_votes where aid=$array[id] and qid=$qid"));
				$width = number_format($array2[votes]/$votes*400);
				$per = number_format($array2[votes]/$votes*100);
				print "<tr>
							<td width=100><FONT color=00000>$array[answer]</td>
							<td width=450><nobr>
								<img src=\"/pix/leftbar.gif\" width=4 height=20 alt=\"\"><img src=\"/pix/mainbar.gif\" width=$width height=20 alt=\"\"><img src=\"/pix/rightbar.gif\" width=4 height=20 alt=\"\"> &nbsp;<FONT color=000000>$array2[votes] / </FONT><FONT color=006666>$per%
							</td>
						</tr>\n";
			}
			print "<tr><td align=right colspan=2><FONT color=000000><B>$votes Total Votes.</td></tr>\n";
			print "</table></FONT>";
		}

		function insert($array, $qid) {
			for($i = 0; $i < count($array); $i++) {
				if($array[$i]) {
					$this->db->query("insert into $this->table (qid, answer) values(\"$qid\", \"$array[$i]\")");
					//print "insert into $this->table (qid, answer) values(\"$qid\", \"$array[$i]\")";
				}
			}
		}
	
	} // end class POLL_ANSWERS

} // end include protection

?>
