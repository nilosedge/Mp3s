<?php

global $FUNC_DORM_TBL_DIRS_INC;
if (!$FUNC_DORM_TBL_DIRS_INC) {
   $FUNC_DORM_TBL_DIRS_INC=1;

   include($INC[db]);

   class DIRS {

      var $db, $resultid;
      var $table, $debug, $level;
		var $spaces;

      function DIRS($debug) {
         global $DORM_DBUSER, $DORM_DBNAME, $DORM_DBHOST, $DORM_DBPASSWD;

			$this->debug = $debug;
         $this->table = "dirs";
         $this->db = new Database($debug);
         $this->db->connect($DORM_DBHOST, $DORM_DBUSER, $DORM_DBPASSWD);
         $this->db->usedatabase($DORM_DBNAME);
			$this->spaces = 0;
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=\"$id\""));
		}

		function print_dirs($parent) {
			$res = $this->db->query("select * from $this->table where parent_id=$parent order by name");

			if($this->db->num_rows($res) > 0) {
				while($array = $this->db->get_array($res)) {
					$this->print_parent($array[id], $array[name]);
					$this->print_child_start($array[id]);
					$this->indent();
					$this->indent();
					$this->print_files($array[id]);
					$this->unindent();
					$this->unindent();
					$this->print_dirs($array[id]);
					$this->print_child_end();
				}
			} else {
				# print fiiles.
			}
		}

		function print_dirs2($par, $lev) {
			$res = $this->db->query("select * from $this->table where parent_id=$par order by name");

			if($par == 0) {
				$this->p("foldersTree = gFld(\"<i>Mp3 Treeview Demo</i>\", \"\")");
				$this->indent();
				$this->p("aux0 = insFld(foldersTree, gFld(\"<i>Mp3 Treeview Demo</i>\", \"\"))");
			}

			if($this->db->num_rows($res) > 0) {
				$lev++;
				while($array = $this->db->get_array($res)) {
					$this->indent();
					$this->p("aux$lev = insFld(aux".($lev - 1).", gFld(\"$array[name]\", \"\"))");
					$this->print_dirs2($array[id], $lev);
					$this->unindent();
				}
			} else {
				# print fiiles.
			}
		}


		function print_files($par_id) {
			$res = $this->db->query("select * from dirs_files where parent_id = $par_id");
			while($array = $this->db->get_array($res)) {
		        $this->ps("<nobr><a class=\"tblItem\" title=\"($array[size] Bytes)\" target=\"phpmain\" href=\"1\">$array[name]</a></nobr><br />");
			}
		}

		function print_child_start($parent) {
			$this->indent();
	   	$this->p("<div id=\"el$parent"."Child\" class=\"child\" style=\"margin-bottom: 5px\">");
		}

		function get_child_count($par_id) {
			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where parent_id = $par_id"));
			return $array[count];
		}

		function get_files_count($par_id) {
			$array = $this->db->get_array($this->db->query("select count(*) as count from dirs_files where parent_id = $par_id"));
			return $array[count];
		}

		function print_child_end() {
			$this->p("</div>");
			$this->unindent();
		}

		function print_head($num, $name) {
			$this->indent();
    		$this->p("<div id=\"el$num"."Parent\" class=\"parent\" style=\"margin-bottom: 5px\">");
			$this->indent();
			$this->ps("<nobr><span class=\"heada\"><b>$name</b></span></nobr>");
			$this->unindent();
			$this->p("</div>");
			$this->unindent();
		}

		function print_parent($num, $name) {
			$num_ch = $this->get_child_count($num);
			$files = $this->get_files_count($num);
			$num_ch += $files;
			$this->indent();
    		$this->p("<div id=\"el$num"."Parent\" class=\"parent\" style=\"margin-bottom: 5px\">");
			$this->indent();
			$this->ps("<nobr><a class=\"item\" href=\"1\" onclick=\"if (capable) {expandBase('el$num', true); return false;}\"><img name=\"imEx\" id=\"el$num"."Img\" src=\"images/plus.gif\" border=\"0\" alt=\"+\" /></a> <a class=\"item\" href=\"1\" onclick=\"if (capable) {expandBase('el$num', false)}\"><span class=\"heada\">$name<bdo dir=\"ltr\">&nbsp;&nbsp;</bdo></span><span class=\"headaCnt\">($num_ch)</span></a></nobr>");
			$this->unindent();
			$this->p("</div>");
			$this->unindent();
                   #<nobr><a class=\"item\" href=\"\"><span class=\"heada\"><b>$name</b></span></a></nobr>
		}

		function indent() { $this->spaces++; }
		function unindent() { $this->spaces--; }

		function p($text) {
			for($i = 0; $i < $this->spaces; $i++) { print " "; }
			print "$text\n";
		}

		function ps($text) {
			for($i = 0; $i < $this->spaces; $i++) { print " "; }
			#for($i = 0; $i < $this->spaces; $i++) { print ""; }
			print "$text\n";
		}
	
	} // end class DIRS

} // end include protection

?>
