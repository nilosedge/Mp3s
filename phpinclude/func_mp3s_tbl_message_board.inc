<?php

global $FUNC_MP3S_TBL_MESSAGE_BOARD_INC;
if (!$FUNC_MP3S_TBL_MESSAGE_BOARD_INC) {
   $FUNC_MP3S_TBL_MESSAGE_BOARD_INC=1;

   include($INC[db]);

   class MESSAGE_BOARD {

      var $db, $resultid;
      var $table, $debug;
		var $maxthreadsize;
		var $searchquery;
		var $totalhits;
		var $pagehits;

      function MESSAGE_BOARD($debug) {
         global $DBUSER, $DBNAME, $DBHOST, $DBPASSWD;

			$this->debug = $debug;
         $this->table = "message_board";
         $this->db = new Database($debug);
         $this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
         $this->db->usedatabase($DBNAME);
			$this->maxthreadsize = 20;
			$this->modv = 0.50;
			$this->pagehits = 20;
			if($debug) print "Max thread size set to $this->maxthreadsize<BR>\n";

			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where pid = 0 and hidden = 0 and modvalue >= $this->modv and thread_count < $this->maxthreadsize"));
			$this->totalhits = $array[count];
      }

		function delete($id) {
			$this->db->query("delete from $this->table where id=\"$id\"");
		}

		function update($id, $name, $email, $subject, $msg) {
			$name = strip_tags($name);
			$email = strip_tags($email);
			$msg = str_replace("\n", "<BR>\n", $msg);
			$this->db->query("update $this->table set name=\"$name\", email=\"$email\", message=\"$msg\", subject=\"$subject\" where id=\"$id\" and name=\"$name\"");
		}

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=\"$id\""));
		}

		function insert_message($array) {
			global $REMOTE_ADDR, $user, $username;

			$username = trim($username);
			$array[name] = trim($array[name]);

			if(($user->user_exists($array[name]) && $username == $array[name]) || !$user->user_exists($array[name])) {

				if($array[name] && $array[message] && $array[subject]) {
					$array[message] = strip_tags($array[message]);
					$array[name] = strip_tags($array[name]);
					$array[email] = strip_tags($array[email]);
					$array[subject] = strip_tags($array[subject]);
					$array[message] = str_replace("\n", "<BR>\n", $array[message]);
					if($array[pid] == 0) {
						$this->db->query("insert into $this->table (pid, name, email, subject, message, ip, time, last_posted, modvalue) values(\"$array[pid]\",\"$array[name]\", \"$array[email]\", \"$array[subject]\", \"$array[message]\", \"$REMOTE_ADDR\", now(), now(), \"$array[modvalue]\")");
					} else {
						$pid = $this->get_root_id($array[pid]);
						$this->db->query("update $this->table set time=time, thread_count=thread_count+1, last_posted=now() where last_posted < now() and id=$pid");
						$this->db->query("insert into $this->table (pid, name, email, subject, message, ip, time, last_posted, modvalue) values(\"$array[pid]\",\"$array[name]\", \"$array[email]\", \"$array[subject]\", \"$array[message]\", \"$REMOTE_ADDR\", now(), now(), \"$array[modvalue]\")");
					}
					return 1;
				} else {
					if($this->debug) print "Not all the things were filled into please don't forget any.<BR>\n";
					return 0;
				}
			} else {
				return 0;
			}
		}

		function search($searchfor, $searchby) {
			global $INC;
			include($INC[parser]);
			$tok = new Parse_Token($searchfor, $this->debug);
			$wa = $tok->get_array();

			$start_query = "select * from $this->table where hidden = 0 and modvalue >= $this->modv and ";
			$middle_query = "(";
			for($i = 0; $i < count($wa); $i++) {
				$middle_query .= " (name like '%$wa[$i]%' or ";
				$middle_query .= "message like '%$wa[$i]%' or ";
				$middle_query .= "subject like '%$wa[$i]%')";
				if($i < (count($wa) - 1)) $middle_query .= " and ";
			}
			$middle_query .= ")";

			$this->searchquery = $start_query.$middle_query;
			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where".$middle_query));
			$this->totalhits = $array[count];
			$this->searchquery .= " order by time desc";
			$this->resultid = $this->db->query($this->searchquery);
			return $this->resultid;

		}

		function display() {
			global $searchfor, $hits, $start, $PAGES;
			$searchfor = urlencode($searchfor);
			$numresult = $this->db->num_rows($this->resultid);

			print "<!-- Starting Table Generation -->\n";
			if ($numresult > 0) {
				print "Thread results, $this->totalhits total Matches.<BR><BR>\n";
				print "<form action=\"$PAGES[result]?searchfor=$searchfor\" method=post>
       <TABLE width=75% border=0 cellpadding=1 cellspacing=1>
        <TR ALIGN=LEFT>
          <TD><U>Subject</U></TD>
          <TD><U>Name</U></TD>
          <TD><U>Time</U></TD>
        </TR>";
			while($res1 = $this->db->get_array($this->resultid)) {
						print "
        <TR>
          <TD><A HREF=\"$PAGES[message_view_thread]?id=$res1[id]\">$res1[subject]</A></TD>
          <TD>$res1[name]</TD>
          <TD>".Date("M d, Y, H:i a", convert_mysql_time($res1[time]))."</TD>
        </TR>\n";
					}
					print "</form>\n       </TABLE>\n";
				}
				else print "Sorry was unable to find anything that matched your request<BR>\n";
				print "<!-- Ending Table Generation -->\n";
		} // end function display 




		function print_top_short($num) {
			global $PAGES;
			$res = $this->db->query("select * from $this->table where pid = 0 and hidden = 0 and modvalue >= $this->modv and thread_count >= $this->maxthreadsize order by last_posted desc limit 0,$num");
			if($this->db->num_rows($res)) {
				print "
					<table cellspacing=0 cellpadding=0 border=0>
						<tr>
							<td colspan=2 align=center><a href=\"$PAGES[message_board_top_threads]\">Last $num Top Threads</a><BR><font size=-1>(People that like to Post)</font></td>
						</tr><!--
						<tr>
							<td>Subject</td>
							<td>Email</td>
						</tr> -->";
				while($array = $this->db->get_array($res)) {
					$count = $this->get_thread_count($array[id]);
					print " <tr><td><font size=-1><b><a href=\"$PAGES[message_view_thread]?id=$array[id]\">$count -- ".stripslashes($array[subject])."</a></b></font></td><td><font size=-1><a href=\"mailto:".$array[email]."\">$array[name]</a></font></td></tr>";
				}
				print "
					</table>";
			}
		}

		function print_top_long($num) {
			global $PAGES;
			$res = $this->db->query("select * from $this->table where pid = 0 and hidden = 0 and modvalue >= $this->modv and thread_count >= $this->maxthreadsize order by time desc limit 0,$num");
			if($this->db->num_rows($res)) {
				print "
					<table>
						<tr>
							<td colspan=2 align=center>Last $num Top Threads<BR><font size=-1>(People that like to Post)</font></td>
						</tr><!--
						<tr>
							<td>Subject</td>
							<td>Email</td>
						</tr> -->";
				while($array = $this->db->get_array($res)) {
					$count = $this->get_thread_count($array[id]);
					print " <tr><td><b><a href=\"$PAGES[message_view_thread]?id=$array[id]\">$count -- ".stripslashes($array[subject])."</a></b></td><td><a href=\"mailto:".$array[email]."\">$array[name]</a></td><td>".Date("M d, Y, H:i a", convert_mysql_time($array[time]))."</td></tr>";
				}
				print "
					</table>";
			}
		}

		function print_most_posted($num) {
			global $PAGES;
			$res = $this->db->query("select * from $this->table where pid = 0 and hidden = 0 and modvalue >= $this->modv and thread_count >= $this->maxthreadsize order by thread_count desc limit 0,$num");
			if($this->db->num_rows($res)) {
				print "
					<table>
						<tr>
							<td colspan=2 align=center>Top $num Threads<BR><font size=-1>(People that have passion)</font></td>
						</tr><!--
						<tr>
							<td>Subject</td>
							<td>Email</td>
						</tr> -->";
				while($array = $this->db->get_array($res)) {
					$count = $this->get_thread_count($array[id]);
					print " <tr><td><b><a href=\"$PAGES[message_view_thread]?id=$array[id]\">$count -- ".stripslashes($array[subject])."</a></b></td><td><a href=\"mailto:".$array[email]."\">$array[name]</a></td></tr>";
				}
				print "
					</table>";
			}
		}

		function print_top_users($num) {
			global $PAGES;
			$res = $this->db->query("select distinct name, sum(length(message)) as len, count(name) as count from $this->table group by name order by count desc limit 0, $num");
			if($this->db->num_rows($res)) {
				print "
					<table border=0 cellspadding=5>
						<tr>
							<td colspan=4 align=center>Top $num Posters</a><BR><font size=-1>(People that REALLY!!! like to Post)</font></td>
						</tr>
						<tr>
							<td>Name</td>
							<td>Count</td>
							<td>Amount</td>
							<td>Avg Post Size</td>
						</tr>";
				while($array = $this->db->get_array($res)) {
					print " <tr><td><b>$array[name]</b></td><td align=right>$array[count]</td><td align=right>".number_format($array[len] / 1024)."K</td><td align=right>".number_format($array[len] / $array[count])." b</tr>";
				}
				print "
					</table>";
			}
		}


		function stats() {

			$array = $this->db->get_array($this->db->query("select count(*) as usercount from users"));
			$info_array[usercount] = $array[usercount];
			$array = $this->db->get_array($this->db->query("select sum(length(subject)) as sub, sum(length(message)) as mes from message_board"));
			$info_array[subject] = $array[sub];
			$info_array[message] = $array[mes];
			$array = $this->db->get_array($this->db->query("select count(*) as count from message_board"));
			$info_array[messagecount] = $array[count];
			$array = $this->db->get_array($this->db->query("select count(*) as count from message_board where pid = 0"));
			$info_array[threadcount] = $array[count];

			print "
					<table>
						<tr><td>Number of Registered users:</td><td align=right>$info_array[usercount]</td></tr>
						<tr><td>Size of all subjects:</td><td align=right>$info_array[subject]</td></tr>
						<tr><td>Size of all messages:</td><td align=right>$info_array[message]</td></tr>
						<tr><td>Number of posts:</td><td align=right>$info_array[messagecount]</td></tr>
						<tr><td>Number of threads:</td><td align=right>$info_array[threadcount]</td></tr>
						<tr><td>Average Posts Per Thread:</td><td align=right>".number_format(($info_array[messagecount] / $info_array[threadcount]), 1)."</td></tr>
					</table>
			";

		}


		function print_last($start, $hits, $pid, $level) {

			global $PAGES;
			global $CONTENT;
			global $username;
			if($pid == 0) {
				if(!$start) $start = 0;
				if(!$hits) $hits = 25;
				$result = $this->db->query("select * from $this->table where pid = $pid and hidden = 0 and modvalue >= $this->modv and thread_count < $this->maxthreadsize order by last_posted desc limit $start,$hits");
				$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where pid = $pid and hidden = 0 and modvalue >= $this->modv and thread_count < $this->maxthreadsize"));
				$this->totalhits = $array[count];
				print "<font size=-1>";
			} else {
				$result = $this->db->query("select * from $this->table where pid = $pid and hidden = 0 and modvalue >= $this->modv order by last_posted");
			}

			if($this->db->num_rows($result)) {

				for($i = 0; $i < $level; $i++) print "\t";
				print "<ul>\n";
				$level++;
				while($array = $this->db->get_array($result)) {

#					if($pid == 0) { 
						//$count = ($this->get_thread_count($array[id]) + 1); 
#						$this->db->query("update $this->table set time=time, thread_count=$count, last_posted=last_posted where id=$array[id]");
#					}

					for($i = 0; $i < $level; $i++) print "\t";
					if($array[name] == $username) $link = "<a href=\"".$CONTENT[pages][message_edit]."?array[id]=$array[id]&start=$start&hits=$hits\">Edit</a>";
					else $link = '';
					$link = '';
					print "<li><b><a href=\"$PAGES[message_view_thread]?id=$array[id]&start=$start&hits=$hits\">".stripslashes($array[subject])."</a></b>,&nbsp;<a name=\"$array[id]\">&nbsp;</a>&nbsp;<a href=\"mailto:".$array[email]."\">$array[name]</a> on ".Date("M d, Y, H:i a", convert_mysql_time($array[time]))."&nbsp;$link</li>\n";
					$this->print_last($start, $hits, $array[id], $level);
					if($pid == 0) print "<BR><BR>\n";
				}
				for($i = 0; $i < $level - 1; $i++) print "\t";
				print "</ul>\n";
			}

			if($pid == 0) print "</font>";
		}



		function print_tree2($start, $hits, $pid, $level) {

			global $PAGES;
			if($pid == 0) {
				if(!$start) $start = 0;
				if(!$hits) $hits = 25;
				$result = $this->db->query("select * from $this->table where pid = $pid and hidden = 0 and modvalue >= $this->modv order by last_posted desc limit $start,$hits");
				print "foldersTree = gFld(\"\", \"\")\n";
				print "aux0 = insFld(foldersTree, gFld(\"<i>Nilo's Message Board</i>\", \"\"))\n";
			} else $result = $this->db->query("select * from $this->table where pid = $pid and hidden = 0 and modvalue >= $this->modv order by last_posted");

			if($this->db->num_rows($result)) {

				$level++;
				while($array = $this->db->get_array($result)) {

					for($i = 0; $i < $level; $i++) print "   ";

					if($pid != 0) $array[thread_count] = "";
					else $array[thread_count] .= " -- "; 

					print "aux$level = insFld(aux".($level - 1).", ";
					print "gFld(\"$array[thread_count]".addslashes($array[subject])."\", \"/$PAGES[message_view_thread]?id=$array[id]\"))\n";
					//print "gFld(\"".addslashes($array[subject])."\", \"\"))\n";
					$this->print_tree2($start, $hits, $array[id], $level);
				}
			}

		}

		function print_last_short($num) {
			//$res = $this->db->query("select * from $this->table where hidden = 0 order by id desc");
			$res = $this->db->query("select * from $this->table where hidden = 0 and modvalue >= $this->modv order by id desc limit 0,100");

			$this->totalhits = 0;
			$this->totalhits = $this->db->num_rows($res);

			if($this->totalhits) {
				print "
					
					<table cellpadding=0 cellspacing=0 border=0>
						<tr>
							<td colspan=2 align=center>Last $num posts</td>
						</tr><!--
						<tr>
							<td>Subject</td>
							<td>Email</td>
						</tr> -->";
				$trys = 0;
				while(($array = $this->db->get_array($res)) && $trys < $num) {
					//print "Id: ".$this->get_root_thread_count($array[id])."<BR>\n";
					if($this->get_root_thread_count($array[id]) < $this->maxthreadsize) {
						print " <tr><td><font size=-1><b><a href=\"#$array[id]\">".stripslashes($array[subject])."</a></b></font></td><td><font size=-1><a href=\"mailto:".$array[email]."\">$array[name]</a></font></td></tr>";
						$trys++;
					}
				}
				print "
					</table>";
			}
		}


		function print_tree($pid, $level) {

			global $CONTENT;
			global $start, $hits;
			global $username;

			$result = $this->db->query("select * from $this->table where hidden = 0 and modvalue >= $this->modv and pid = $pid order by id");

			if($this->db->num_rows($result)) {

				for($i = 0; $i < $level; $i++) print "\t";
				print "<ul>\n";
				$level++;
				while($array = $this->db->get_array($result)) {
					for($i = 0; $i < $level; $i++) print "\t";
					if($array[name] == $username) $link = "<a href=\"".$CONTENT[pages][message_edit]."?array[id]=$array[id]&start=$start&hits=$hits\">Edit</a>";
					else $link = '';
					$link = '';
					print "<li><b><a href=\"".$CONTENT[pages][message_view_thread]."?id=$array[id]&start=$start&hits=$hits\">".stripslashes($array[subject])."</a></b>,&nbsp;<a name=\"$array[id]\">&nbsp;</a>&nbsp;<a href=\"mailto:".$array[email]."\">$array[name]</a> on ".Date("M d, Y, H:i a", convert_mysql_time($array[time]))."&nbsp;$link</li>\n";
					$this->print_tree($array[id], $level);
				}
				for($i = 0; $i < $level - 1; $i++) print "\t";
				print "</ul>\n";
			}
		}

		function get_root_id($id) {

			$result = $this->db->query("select * from $this->table where id = $id");
			if($this->db->num_rows($result)) {
				$array = $this->db->get_array($result);
				if($array[pid] != 0) {
					return $this->get_root_id($array[pid]);
				} else {
					return $array[id];
				}
			} else {
				print "Bad Bad Error occured: func_mp3s_tbl_message_board.inc:get_root_id().<BR>\n";
			}
		}

		function get_thread_count($id) {

			$result = $this->db->query("select * from $this->table where pid = $id");
			if($count = $this->db->num_rows($result)) {
				while($array = $this->db->get_array($result)) {
					$count += $this->get_thread_count($array[id]);
				}
				return $count;
			} else {
				#print "Bad Bad Error occured: func_mp3s_tbl_message_board.inc:get_thread_count().<BR>\n";
				return 0;
			} 
		}

		function get_root_thread_count($id) {
			$result = $this->db->query("select * from $this->table where id = $id");
			if($this->db->num_rows($result)) {
				$array = $this->db->get_array($result);
				if($array[pid] != 0) {
					return $this->get_root_thread_count($array[pid]);
				} else {
					return $array[thread_count];
				}
			} else {
				print "Bad Bad Error occured: func_mp3s_tbl_message_board.inc:get_root_thread_count().<BR>\n";
			}
		}

		function print_seq_last($start, $hits) {

			global $PAGES;
			if(!$start) $start = 0;
			if(!$hits) $hits = $this->pagehits;

			$result = $this->db->query("select * from $this->table where hidden=0 and modvalue >= $this->modv order by id desc limit $start,$hits");
	
			$this->totalhits = 0;
			$this->totalhits = $this->db->num_rows($result);

			if($this->totalhits) {
				print "
					<table border=1 width=90%>
						<tr>
							<td>Name</td>
							<td>Subject</td>
							<td>Message</td>
							<td>Time</td>
							<td>Respond</td>
						</tr>
				";

				while($array = $this->db->get_array($result)) {
					print "
						<tr>
							<td><a href=\"mailto:$array[email]\">$array[name]</a></td>
							<td>$array[subject]</td>
							<td>$array[message]</td>
							<td><nobr>".Date("M d, Y, H:i a", convert_mysql_time($array[time]))."</nobr></td>
							<td><a href=\"$PAGES[message_new]?rid=$array[id]&hits=$hits&start=$start\">Respond</a></td>
						</tr>
					";
				}
				print "
					</table>
				";
			}

		}


		function print_myposts($start, $hits) {

			global $CONTENT, $username;
			if(!$start) $start = 0;
			if(!$hits) $hits = $this->pagehits;

			$result = $this->db->query("select * from $this->table where name=\"$username\" order by id desc limit $start,$hits");
			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where name=\"$username\""));
			$this->totalhits = $array[count];

			if($this->db->num_rows($result)) {
				print "
					<table border=1 width=90%>
						<tr>
							<td>Name</td>
							<td>Subject</td>
							<td>Message</td>
							<td>Time</td>
							<td>Moderation Value</td>
							<td>Respond</td>
						</tr>
				";

				while($array = $this->db->get_array($result)) {
					if(!trim($array[message])) $array[message] = "&nbsp;";
					print "
						<tr>
							<td><a href=\"mailto:$array[email]\"><nobr>$array[name]</nobr></a></td>
							<td>$array[subject]</td>
							<td>$array[message]</td>
							<td><nobr>".Date("M d, Y, H:i a", convert_mysql_time($array[time]))."</nobr></td>
							<td>".($array[modvalue] * 100)."%</td>
							<td><a href=\"".$CONTENT[pages][message_edit]."?array[id]=$array[id]&hits=$hits&start=$start\">Edit</a></td>
						</tr>
					";
				}
				print "
					</table>
				";
			}

		}

		function update_modvalue($id, $val) {
			$this->db->query("update $this->table set modvalue = modvalue + $val where id = $id");
		}

		function print_unmod($uid) {

			global $CONTENT;
			global $moded;
			global $modarray;

			$res = $this->db->query("select * from $this->table where modvalue < 1 and modvalue >= $this->modv and hidden = 0");

			print "<table border=1><tr><td>Subject</td><td>Name</td><td>Moderation Values</td><td>Action</td><td>You Have<BR>Already Moderated</td></tr>\n";

			while($array = $this->db->get_array($res)) {
				if($moded->exists($modarray[id], $array[id])) $voted = "Yes";
				else $voted = "No";
				print "<tr><td>$array[subject]</td><td>$array[name]</td><td>".($array[modvalue] * 100)."%</td><td><a href=\"".$CONTENT[pages][message_view]."?array[id]=$array[id]\">Read Message</a><td align=right>$voted</td></tr>\n";
			}
			print "</table>";

		}


		function print_numbers($start, $hits) {

			global $CONTENT;
			global $COLORS, $PHP_SELF;

			if(!$start) $start = 0;
			if(!$hits) $hits = $this->pagehits;
			$amount = 10;
	
				if ($this->totalhits > $hits) {
					print "       <BR><BR>\n       <CENTER>\n";
					print "       <TABLE>\n         <TR>\n";

					$number = (int)($start / ($amount * $hits));


					if (($hits * ($number - 1) * $amount) >= 0) {
						print "<TD>&nbsp;<A HREF=\"$PHP_SELF?start=".($amount * ($number - 1) * $hits)."&hits=$hits\"><<</A></TD>\n";
					} else {
						print "           <TD>&nbsp;<FONT color=000000><<</FONT></TD>\n";
					}

					if ($start > 0) print "           <TD>&nbsp;<A HREF=\"$PHP_SELF?start=".($start - $hits)."&hits=$hits\"><</A></TD>\n";
					else print "           <TD>&nbsp;<FONT color=000000><</FONT></TD>\n";

					$counter = 0;
					if($this->debug > 1) print "This is the start $start<BR>\n";

					for($i = ($hits * $number * $amount); $i < $this->totalhits && $counter < $amount; $i += $hits) {
						$counter++;
						$color = $COLORS[$CONTENT[colors][$CONTENT[name][basename($PHP_SELF)]]][link];
						print "           <td>";

						$gen = (($number * $amount) + $counter);

						if($i == $start) print "<font color=$color size=+4><b>$gen</b></font>";
						else {
							print "<A HREF=\"$PHP_SELF?hits=$hits&start=$i\">";
							$string = "$gen";
							for ($q = 0; $q < strlen($string); $q++) {
								print "<IMG SRC=\"pix/$string[$q].jpg\" border=0 ALT=\"$gen\">";
							}
							print "</a>";
						}
						print "&nbsp;</TD>\n";
					}
					if ($this->totalhits > ($hits + $start)) print "           <TD><A HREF=\"$PHP_SELF?start=".($start + $hits)."&hits=$hits\" ALT=\"Next\">></A></TD>\n";
					else print "           <TD><FONT color=000000>></FONT></TD>\n";

					if ($this->totalhits > ($hits * ($number + 1) * $amount)) print "           <TD><A HREF=\"$PHP_SELF?start=".($amount * ($number + 1) * $hits)."&hits=$hits\" ALT=\"Next\">>></A></TD>\n";
					else print "           <TD><FONT color=000000>>></FONT></TD>\n";


					print "         </TR>\n       </TABLE>";
				}

		}


		function print_numbers_list($start, $hits) {

			if(!$start) $start = 0;
			if(!$hits) $hits = $this->pagehits;
			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table"));

			$result = $this->db->query("select * from $this->table order by time desc limit $start,$hits");
			global $php_self;
			global $searchfor;
			global $pages;
			global $colors;
			global $page_colors;
			print "       <br><br>\n       <center>\n";
			print "       <table>\n         <tr>\n";
				if ($array[count] > $hits) {
					if ($start > 0) print "           <td><a href=\"$php_self?start=".($start - $hits)."&hits=$hits\"><--prev</a></td>\n";
					else print "           <td><font color=000000><--prev</font></td>\n";
					$counter = 0;
					if($this->debug > 1) print "this is the start $start<br>\n";
					for($i = 0; $i < $array[count] && $counter < 30; $i += $hits) {
						$counter++;
						$color = $colors[$page_colors[basename($php_self)]][link];
						print "<td>\n";
						if($i == $start) print "           <font color=$color size=+4><b>$counter</b></font>";
						else {
							print "           <a href=\"$php_self?hits=$hits&start=$i\">";
							$string = "$counter";
							for ($q = 0; $q < strlen($string); $q++) {
								print "<img src=\"pix/$string[$q].jpg\" border=0 alt=\"$counter\">";
							}
							print "</a>\n";
						}
						print "&nbsp;</td>\n";
					}
					if ($array[count]> ($hits + $start)) print "           <td><a href=\"$php_self?start=".($start + $hits)."&hits=$hits\" alt=\"next\">next--></a></td>\n";
					else print "           <td><font color=000000>next--></font></td>\n";
				}
			print "         </tr>\n       </table>";

		} // end function print_numbers_list
	
	} // end class MESSAGE_BOARD

} // end include protection

?>
