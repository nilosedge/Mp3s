<?php

	global $FUNC_MP3S_TBL_TOP_DOWNLOAD_INC;
	if(!$FUNC_MP3S_TBL_TOP_DOWNLOAD_INC) {
		$FUNC_MP3S_TBL_TOP_DOWNLOAD_INC = 1;

		include($INC[db]);

		class topdownload {

	   	var $db, $resultid, $table, $debug;

			function TOPDOWNLOAD($debug) {
				global $DBUSER, $DBNAME, $DBHOST, $DBPASSWD;
		      $this->db = new Database($debug);
		      $this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
		      $this->db->usedatabase($DBNAME);
				$this->table = "top_downloaded";
				$this->debug = $debug;
			} // end constructor function 

			function get_list() {
				$result = $this->db->query("select * from $this->table order by hits desc limit 0,5");
				while($array = $this->db->get_array($result)) {
					$return[$array[songname]] = $array[hits];
				}
				return $return;
			} // end function get_list()

		} // end class download_log

	} // end include protection
?>
