<?php

global $FUNC_TABLE_PLAYING_INC;
if(!$FUNC_TABLE_PLAYING_INC) {
	$FUNC_TABLE_PLAYING_INC = 1;
	
	include($INC[db]);

	class PLAYING {

		var $table, $debug;

		function PLAYING($debug) {
			global $DBNAME, $DBUSER, $DBHOST, $DBPASSWD;
			$this->debug = $debug;
			$this->table = "playing";
			$this->db = new Database($debug);
			$this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
			$this->db->usedatabase($DBNAME);
		} // end constructor playlist

		function get_playing() {
			return $this->db->get_array($this->db->query("select id, mp3_id, unix_timestamp(time) as time from $this->table where id=1"));
		}

		function next() {
			$this->db->query("update playing set next=1 where id=1");
		}

	} // end class PLAYING

} // end include protection

?>
