#!/usr/bin/perl -w



print "Please enter the database: ";

$database = <>;
chomp $database;

print "Please enter the table: ";
$table = <>;
chomp $table;

$file = "func_$database"."_tbl_$table".".inc";

open(OUT, "> $file") or die "Can't open file $!\n";
print OUT "<?php

global \$FUNC_".uc($database)."_TBL_".uc($table)."_INC;
if (!\$FUNC_".uc($database)."_TBL_".uc($table)."_INC) {
   \$FUNC_".uc($database)."_TBL_".uc($table)."_INC=1;

   include(\$INC[db]);

   class ".uc($table)." {

      var \$db, \$resultid;
      var \$table, \$debug;

      function ".uc($table)."(\$debug) {
         global \$DBUSER, \$DBNAME, \$DBHOST, \$DBPASSWD;

			\$this->debug = \$debug;
         \$this->table = \"$table\";
         \$this->db = new Database(\$debug);
         \$this->db->connect(\$DBHOST, \$DBUSER, \$DBPASSWD);
         \$this->db->usedatabase(\$DBNAME);
      }

		function get_info(\$id) {
			return \$this->db->get_array(\$this->db->query(\"select * from \$this->table where id=\$id\"));
		}
	
	} // end class ".uc($table)."

} // end include protection

?>";
close(OUT);

