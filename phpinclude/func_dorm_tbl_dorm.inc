<?php

global $FUNC_DORM_INC;
if (!$FUNC_DORM_INC){
	$FUNC_DORM_INC=1;

include($INC[db]);
include($INC[parser]);

class DORM {

   var $db, $resultid, $searchquery, $table; 
	var $searchstring, $totalhits;
	var $hits, $start, $debug;

   function DORM($debug){
		global $DORM_DBNAME, $DORM_DBUSER, $DORM_DBHOST, $DORM_DBPASSWD;
		$this->debug = $debug;
		$this->table = "files";
		$this->db = new Database($debug);
		$this->db->connect($DORM_DBHOST, $DORM_DBUSER, $DORM_DBPASSWD);
		$this->db->usedatabase($DORM_DBNAME);
   }


   function search($searchstring, $start, $hits) {
		$searchstring = stripslashes($searchstring);
		$searchstring = str_replace("*", "%", $searchstring);
		global $FILELIST;
		global $INC;
		$this->searchstring = $searchstring;
		if (!$start) $start = 0;
		if (!$hits) $hits = 10;
		$this->start = $start;
		$this->hits = $hits;

		include($INC[skipwords]);

		$skip = new SKIPWORDS($this->debug);


		$tok = new parse_token($this->searchstring);

		$array = $skip->chop_words($tok->get_array());

		# $array = split(" ", $this->searchstring);
		$keyword = 2;

		if($array && $keyword == 0) {
			$query = "select * ";
			$totalquery = "select count(*) as count ";
			$query .= "from $this->table where ";
			$totalquery .= "from $this->table where ";
			for($i = 0; $i < count($array); $i++) {
				if(!$array[$i]) continue;
				$query .= "concat(file_path,file_name) like '%$array[$i]%' ";
				$totalquery .= "concat(file_path,file_name) like '%$array[$i]%' ";
				if($i < (count($array) - 1)) { $query .= " && "; $totalquery .= " && "; }
			}
			$query .= " order by host_id limit $this->start, $this->hits";

			if($this->debug) print "Query: $query<BR>\n";

			$array = $this->db->get_array($this->db->query($totalquery));
			$this->totalhits = $array[count];
			# $this->totalhits = `/bin/cat $filelist | $grep | /usr/local/bin/lines`;
			if($this->totalhits > 0) {
				$this->resultid = $this->db->query($query);		
			} else $this->resultid = 0;
		}

		if($array && $keyword == 1) {
			$query = 'select hosts.name as host_name, concat(\'file://\', hosts.ip, \'/\', shares.name, \'/\', files.file_path) as path, files.file_name, files.size, files.extention ';
			$totalquery = "select count(*) as count ";
			$query       .= "from $this->table, keyword, hosts, shares where files.id=keyword.file_id and files.share_id=shares.id and files.host_id=hosts.id and hosts.name !='' and (";
			$totalquery  .= "from $this->table, keyword, hosts, shares where files.id=keyword.file_id and files.share_id=shares.id and files.host_id=hosts.id and hosts.name !='' and (";
	
			for($i = 0; $i < count($array); $i++) {
				// if(!$array[$i]) continue;
				$query .= "word = '$array[$i]' ";
				$totalquery .= "word = '$array[$i]' ";
				if($i < (count($array) - 1)) { $query .= " or "; $totalquery .= " or "; }
			}
			$query .= ") group by file_id having count(*) > ".(count($array) - 1)." limit $this->start, $this->hits";
			$totalquery .= ") group by file_id having count(*) > ".(count($array) - 1);

			if($this->debug) print "Query: $query<BR>\n";


			$res = $this->db->query($totalquery);
			#$array = $this->db->get_array($this->db->query("select ".$this->db->num_rows($res)." as count"));
			#$this->totalhits = $array[count];
			$this->totalhits = $this->db->num_rows($res);

			# $this->totalhits = `/bin/cat $filelist | $grep | /usr/local/bin/lines`;
			if($this->totalhits > 0) {
				$this->resultid = $this->db->query($query);		
			} else $this->resultid = 0;
		}

		if($array && $keyword == 2) {
			$query = 'select file_id from keyword where (';
			$totalquery = 'select file_id from keyword where (';

			for($i = 0; $i < count($array); $i++) {
				// if(!$array[$i]) continue;
				$query .= "word = '$array[$i]' ";
				$totalquery .= "word = '$array[$i]' ";
				if($i < (count($array) - 1)) { $query .= " or "; $totalquery .= " or "; }
			}
			$query .= ") group by file_id having count(*) > ".(count($array) - 1)." limit $this->start, $this->hits";
			$totalquery .= ") group by file_id having count(*) > ".(count($array) - 1);

			$this->totalhits = $this->db->num_rows($this->db->query($totalquery));

			$res = $this->db->query($query);

			$hits = $this->db->num_rows($res);

			$query = 'select hosts.name as host_name, concat(\'file://\', hosts.ip, \'/\', shares.name, \'/\', files.file_path) as path, files.file_name, files.size, files.extention ';
			$query .= "from $this->table, hosts, shares where files.share_id=shares.id and files.host_id=hosts.id and (";
			for($count = 0; $a = $this->db->get_array($res); $count++) {
				$query .= "files.id = $a[file_id] ";
				if($count < ($hits - 1)) { $query .= " or "; }	
			}
			$query .= ")";

			$this->resultid = $this->db->query($query);

		}

		if($array && $keyword == 3) {
			$query = 'select file_id from keyword where (';
			$totalquery = 'select file_id from keyword where (';

			for($i = 0; $i < count($array); $i++) {
				// if(!$array[$i]) continue;
				$mya = $this->db->get_array($this->db->query("select id from words where word='$array[$i]'"));
				$query .= "KeyWordId=$mya[id] ";
				$totalquery .= "KeyWordId=$mya[id] ";
				if($i < (count($array) - 1)) { $query .= " or "; $totalquery .= " or "; }
			}
			$query .= ") group by file_id having count(*) > ".(count($array) - 1)." limit $this->start, $this->hits";
			$totalquery .= ") group by file_id having count(*) > ".(count($array) - 1);

			$this->totalhits = $this->db->num_rows($this->db->query($totalquery));

			$res = $this->db->query($query);

			$hits = $this->db->num_rows($res);

			$query = 'select hosts.name as host_name, concat(\'file://\', hosts.ip, \'/\', shares.name, \'/\', files.file_path) as path, files.file_name, files.size, files.extention ';
			$query .= "from $this->table, hosts, shares where files.share_id=shares.id and files.host_id=hosts.id and (";
			for($count = 0; $a = $this->db->get_array($res); $count++) {
				$query .= "files.id = $a[file_id] ";
				if($count < ($hits - 1)) { $query .= " or "; }	
			}
			$query .= ")";

			$this->resultid = $this->db->query($query);

		}

   }

   function display($layout){
		if (!($layout)) { $layout = "long"; }
      if ($layout == "long") {
			if(!$this->resultid) $numresult = 0;
			else $numresult = $this->db->num_rows($this->resultid);

			print "<!-- Starting Table Generation -->\n";
			if ($numresult) {
				if (($this->start + $this->hits) > ($this->start + $numresult)) {
					$temp = $this->start + $numresult;
				}
				else { $temp = $this->start + $this->hits; }
				$bgcolor = "333333";
				print "<H1>Dorm Search Page</H1>\n";
				print "Unfortunatly IE is the only browser that will get these files correctly<BR>\n";
				print "Dorm results ".($this->start + 1)." - ".($temp)." of $this->totalhits total matches for: $this->searchstring<BR><BR>\n";
            for($step = 0; $step < $numresult; $step++){
               if( !$step ) {
                  print "       <TABLE bgcolor=$bgcolor width=100% border=1 cellpadding=1 cellspacing=1>
         <TR ALIGN=CENTER>
				<TD>Download</TD>";	
           print "<TD><U>Host</U></TD>\n";
			  print "<TD bgcolor=ffffff>_</TD>\n";
           print "<TD><U>Path</U></TD>\n";
			  print "<TD>Explore</TD>\n";
           print "<TD><U>Size in Bytes</U></TD>
         </TR>";
               }
               $res1 = $this->db->get_array($this->resultid);

					#$folder = ereg_replace($res1[file_name], "", $res1[file_path]);
					#print "This is the folder >$folder<<BR>\n";	
					#print "This is the file >$res1[file_name]<<BR>\n";	
					#$share = $this->get_sharename($res1[share_id]);
					#$hostname = $this->get_hostname($res1[host_id]);
					$hostname = $res1[host_name];
					$share = $res1[share_name];
					$ip = $res1[ip];
					#$ip = $this->get_hostip($res1[host_id]);
					//$res1[file_path] = addslashes($res1[file_path]);
					#if($res1[file_path]) $folder = '\\\\'.$ip.'\\'.$share.'\\'.$res1[file_path];
					#else $folder = "\\\\$ip\\$share";
					#$full_path = $folder."\\$res1[file_name]";
					$folder = $res1[path];
					$full_path = $res1[path].$res1[file_name];
               print "
         <TR>
           <TD><A HREF=\"$full_path\" target=new>Download</A></TD>\n";
			  print "<TD>$hostname</TD>\n";
			  print "<TD bgcolor=ffffff>";
			  if(@is_file("pix/$res1[extention].gif")) print "<img src=\"pix/$res1[extention].gif\" alt=\"\">";
			  else print "<img src=\"pix/h_default.gif\" alt=\"Default\">";
			  print "</td>\n";
			  print "<TD>$full_path</TD>\n";
			  print "<TD><A HREF=\"$folder\" target=new><img src=\"pix/explore_c.gif\" border=0 alt=\"Explore\"></A></TD>\n";
           print "<TD ALIGN=RIGHT>$res1[size]</TD>
         </TR>\n";
		}
      print "       </TABLE>\n";
		$this->displaynumbers();
		}
		else {
			print "Sorry was unable to find anything that matched your request<BR>\n";
			$this->displaysearch();
		}
		print "<!-- Ending Table Generation -->\n";
	 }

	 else {
            print "<FONT COLOR=#00FF00>No layout or unknown layout: $layout";	    
	 }




   } 

	function displaysearch() {
		global $PHP_SELF, $PAGES;
	   print "
       <FORM method=GET action=\"$PHP_SELF\">
         <FONT color=\"000000\">
			<INPUT type=text name=searchfor size=31 maxlength=256 value=\"".htmlentities(stripslashes($this->searchstring), ENT_COMPAT)."\"> 
			<INPUT type=hidden name=hits value=\"$this->hits\">
			<INPUT type=hidden name=dorm value=\"1\">
			<INPUT name=search type=submit value=\"Dorm Search\"></FONT>
       </FORM>\n";
	}

	function get_sharename($id) {
		$array = $this->db->get_assarray($this->db->query("select name from shares where id=\"$id\""));
		return $array[name];
	}

	function get_hostname($id) {
		$array = $this->db->get_assarray($this->db->query("select name from hosts where id=\"$id\""));
		return $array[name];
	}
	function get_hostip($id) {
		$array = $this->db->get_assarray($this->db->query("select ip from hosts where id=\"$id\""));
		return $array[ip];
	}

	function displaynumbers() {
		global $PAGES;
		global $COLORS;
		print "       <BR><BR>\n       <CENTER>\n";
		print "       <TABLE>\n         <TR>\n";
		if ($this->totalhits > $this->hits) {
			if ($this->start > 0) {
				print "           <TD><A HREF=\"$PAGES[result]?dorm=1&searchfor=".rawurlencode($this->searchstring)."&start=".($this->start - $this->hits)."&hits=$this->hits\"><--Prev</A></TD>\n";
			} else print "           <TD><FONT color=000000><--Prev</FONT></TD>\n";
			$counter = 0;
			for($i = 0; $i < $this->totalhits && $counter < 20; $i += $this->hits) {
				$counter++;
				$color = $COLORS[darkblue][link];
				print "<td>\n";
				if($i == $this->start) print "         <font color=$color size=+4><b>$counter</b></font>";
				else {
					print "           <A HREF=\"$PAGES[result]?dorm=1&searchfor=".rawurlencode($this->searchstring)."&hits=$this->hits&start=$i\">";
					$string = "$counter";
					for ($q = 0; $q < strlen($string); $q++) {
						print "<IMG SRC=\"pix/$string[$q].jpg\" border=0 ALT=\"$counter\">";
					} 
					print "</a>\n";
				}
				print "&nbsp;</TD>\n";
			}
			if ($this->totalhits > ($this->hits + $this->start)) {
			print "           <TD><A HREF=\"$PAGES[result]?dorm=1&searchfor=".rawurlencode($this->searchstring)."&start=".($this->start + $this->hits)."&hits=$this->hits\" ALT=\"Next\">Next--></A></TD>\n";
			} else print "           <TD><FONT color=000000>Next--></FONT></TD>\n";
		} print "         </TR>\n       </TABLE>";
		$this->displaysearch();
	} // end function display

} // end class dorm

} // end include protection

?>
