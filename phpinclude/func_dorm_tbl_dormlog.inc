<?php

global $FUNC_DORMLOG_INC;
if (!$FUNC_DORMLOG_INC){
	$FUNC_DORMLOG_INC=1;
	include($INC[db]);

	class DORMLOG {

	var $db, $resultid;
	var $table, $am;

	function DORMLOG($debug){
	global $DORM_DBUSER, $DORM_DBNAME, $DORM_DBHOST, $DORM_DBPASSWD;

		$this->table = "dormlogs";	
		$this->db = new Database($debug);
		$this->db->connect($DORM_DBHOST, $DORM_DBUSER, $DORM_DBPASSWD);
		$this->db->usedatabase($DORM_DBNAME);
		$this->am = 10;
	}

		function store($searchfor, $results, $ip) {
	if(!$this->exists($searchfor, $ip)) {
		$hostname = @gethostbyaddr($ip);
		$this->db->query("LOCK TABLES $this->table WRITE");
		$this->db->query("insert into $this->table (searchfor, results, ip, hostname) values(\"$searchfor\", \"$results\", \"$ip\", \"$hostname\")");
		$this->db->query("UNLOCK TABLES");
	}
		}

		function total_searches() {
			$array = $this->db->get_array($this->db->query("select count(*) as number from dormlogs"));
			return $array[number];
		}

		function exists($s, $ip) {
			if(!$s) return 1;
			return $this->db->num_rows($this->db->query("select * from $this->table where searchfor like '%$s%' and ip=\"$ip\""));
		}

		function get_all() {
			return $this->db->get_array($this->db->query("select * from indexing"));
		}

		function getname($host_id) {
			return $this->db->get_array($this->db->query("select * from hosts where id=\"$host_id\""));
		}

		function get_start_time() {
			$array = $this->db->get_array($this->db->query("select start as time from indexing"));
			return $array[time];
		}

		function get_stop_time() {
			$array = $this->db->get_array($this->db->query("select stop as time from indexing"));
			return $array[time];
		}

		function get_total_file_count() {
			$array = $this->db->get_array($this->db->query("select file_count as time from indexing"));
			return $array[time];
		}
		
		function get_current_file_count() {
			$array = $this->db->get_array($this->db->query("select count(id) as time from files"));
			return $array[time];
		}

		function get_total_size() {
			$array = $this->db->get_array($this->db->query("select size from dormsize"));
			return $array[size];
		}

		function get_running_time() {
			$array = $this->db->get_array($this->db->query("select last_running_time as time from indexing"));
			return $array[time];
		}

		function run_index() {
			$array = $this->db->get_array($this->db->query("select running_index from indexing"));
			return $array[running_index];	
		}

		function print_topmpgs() {
			# global $totalvids, $totalvidsize;
			print "Top $this->am People Sharing Movies<BR><BR>\n";
			$this->resultid = $this->db->query("select * from top_mpgs order by id limit 0,$this->am");
			print "\t\t\t\t\t\t\t<TABLE border=0 width=80%>
			<TR ALIGN=CENTER><TD>Host Name</TD><TD>Amount</TD><TD>Sharing Size</TD><TD>Average File Size</TD></TR>\n";
			while($res2 = $this->db->get_array($this->resultid)) {
				# if(!($res2[amount] > $totalvids) && !$dis) { $dis = 1; print "\t\t\t\t\t\t\t<TR><TD><FONT color=0000ff>Nilo</font></TD><TD align=right>".number_format($totalvids)."</TD><td align=right>".number_format(($totalvidsize / (1024 * 1024 * 1024)), 2)."GB</td></TR>\n"; }
				# $array = $this->getname($res2[host_id]);
				$res2[av] = number_format((($res2[size] / $res2[amount]) / (1024 * 1024)), 2);
				$res2[size] = number_format($res2[size] / (1024 * 1024 * 1024), 2);
				//if($res2[name] == "NILO") print "\t\t\t\t\t\t\t\t<TR><TD><FONT color=ff0000>Nilo</font></td><TD align=right>".number_format($res2[amount])."</td><td align=right>$res2[size]GB</td><td align=right>$res2[av]MB</td></TR>\n";
				//else print "\t\t\t\t\t\t\t\t<TR><TD><A href=\"\\\\$res2[ip]\">$res2[name]</a></td><TD align=right>".number_format($res2[amount])."</td><td align=right>$res2[size]GB</td><td align=right>$res2[av]MB</td></TR>\n";
				print "\t\t\t\t\t\t\t\t<TR><TD><A href=\"file://$res2[ip]\">$res2[name]</a></td><TD align=right>".number_format($res2[amount])."</td><td align=right>$res2[size]GB</td><td align=right>$res2[av]MB</td></TR>\n";
			}
			print "\t\t\t\t\t\t\t</TABLE>\n";
		}

		function print_topeml() {
			# global $totalvids, $totalvidsize;
			print "<BR>Top $this->am People Sharing Virus'<BR><BR>\n";
			$this->resultid = $this->db->query("select * from top_eml order by id limit 0,$this->am");
			print "\t\t\t\t\t\t\t<TABLE border=0 width=80%>
			<TR ALIGN=CENTER><TD>Host Name</TD><TD>Amount</TD><TD>Sharing Size</TD><TD>Average File Size</TD></TR>\n";
			while($res2 = $this->db->get_array($this->resultid)) {
				# if(!($res2[amount] > $totalvids) && !$dis) { $dis = 1; print "\t\t\t\t\t\t\t<TR><TD><FONT color=0000ff>Nilo</font></TD><TD align=right>".number_format($totalvids)."</TD><td align=right>".number_format(($totalvidsize / (1024 * 1024 * 1024)), 2)."GB</td></TR>\n"; }
				# $array = $this->getname($res2[host_id]);
				$res2[av] = number_format((($res2[size] / $res2[amount]) / (1024)), 2);
				$res2[size] = number_format($res2[size] / (1024 * 1024), 2);
				//if($res2[name] == "NILO") print "\t\t\t\t\t\t\t\t<TR><TD><FONT color=ff0000>Nilo</font></td><TD align=right>".number_format($res2[amount])."</td><td align=right>$res2[size]GB</td><td align=right>$res2[av]MB</td></TR>\n";
				//else print "\t\t\t\t\t\t\t\t<TR><TD><A href=\"\\\\$res2[ip]\">$res2[name]</a></td><TD align=right>".number_format($res2[amount])."</td><td align=right>$res2[size]MB</td><td align=right>$res2[av]KB</td></TR>\n";
				print "\t\t\t\t\t\t\t\t<TR><TD><A href=\"file://$res2[ip]\">$res2[name]</a></td><TD align=right>".number_format($res2[amount])."</td><td align=right>$res2[size]MB</td><td align=right>$res2[av]KB</td></TR>\n";
			}
			print "\t\t\t\t\t\t\t</TABLE>\n";
		}

		function print_toppix() {
			# global $totalvids, $totalvidsize;
			print "<BR>Top $this->am People Sharing Pictures<BR><BR>\n";
			$this->resultid = $this->db->query("select * from top_pix order by id limit 0,$this->am");
			print "\t\t\t\t\t\t\t<TABLE border=0 width=80%>
			<TR ALIGN=CENTER><TD>Host Name</TD><TD>Amount</TD><TD>Sharing Size</TD><TD>Average File Size</TD></TR>\n";
			while($res2 = $this->db->get_array($this->resultid)) {
				# if(!($res2[amount] > $totalvids) && !$dis) { $dis = 1; print "\t\t\t\t\t\t\t<TR><TD><FONT color=0000ff>Nilo</font></TD><TD align=right>".number_format($totalvids)."</TD><td align=right>".number_format(($totalvidsize / (1024 * 1024 * 1024)), 2)."GB</td></TR>\n"; }
				# $array = $this->getname($res2[host_id]);
				$res2[av] = number_format((($res2[size] / $res2[amount]) / (1024 * 1024)), 2);
				$res2[size] = number_format($res2[size] / (1024 * 1024 * 1024), 2);
				if($res2[name] == "NILO") print "\t\t\t\t\t\t\t\t<TR><TD><FONT color=ff0000>Nilo</font></td><TD align=right>".number_format($res2[amount])."</td><td align=right>$res2[size]GB</td><td align=right>$res2[av]MB</td></TR>\n";
				else print "\t\t\t\t\t\t\t\t<TR><TD><A href=\"\\\\$res2[ip]\">$res2[name]</a></td><TD align=right>".number_format($res2[amount])."</td><td align=right>$res2[size]GB</td><td align=right>$res2[av]MB</td></TR>\n";
			}
			print "\t\t\t\t\t\t\t</TABLE>\n";
		}



		function print_topmp3s() {
			# global $totalmp3s, $totalsize;
			print "Top $this->am People Sharing Music<BR><BR>\n";
			$this->resultid = $this->db->query("select * from top_mp3s order by id limit 0,$this->am");
			if($this->db->num_rows($this->resultid) > 0) {
				print "\t\t\t\t\t\t\t<TABLE border=0 width=80%>
				<TR ALIGN=CENTER><TD>Host Name</TD><TD>Amount</TD><TD>Sharing Size</TD><TD>Average File Size</TD></TR>\n";
				while($res2 = $this->db->get_array($this->resultid)) {
					# if(!($res2[amount] > $totalmp3s) && !$dis) { $dis = 1; print "\t\t\t\t\t\t\t<TR><TD><FONT color=0000ff>Nilo</font></TD><TD align=right>".number_format($totalmp3s)."</TD><td align=right>".number_format(($totalsize / (1024 * 1024 * 1024)), 2)."GB</td></TR>\n"; }
					$res2[av] = number_format((($res2[size] / $res2[amount]) / (1024 * 1024)), 2);
					$res2[amount] = number_format($res2[amount]);
					$res2[size] = number_format($res2[size] / (1024 * 1024 * 1024), 2);
					if($res2[name] == "NILO") print "\t\t\t\t\t\t\t\t<TR><TD><FONT color=ff0000>Nilo</font></td><TD align=right>$res2[amount]</td><td align=right>$res2[size]GB</td><td align=right>$res2[av]MB</td></TR>\n";
					else print "\t\t\t\t\t\t\t\t<TR><TD><A href=\"\\\\$res2[ip]\">$res2[name]</a></td><TD align=right>$res2[amount]</td><td align=right>$res2[size]GB</td><td align=right>$res2[av]MB</td></TR>\n";
				}
				print "\t\t\t\t\t\t\t</TABLE>\n";
			}
		}

		function print_top10() {
			global $PAGES;
			print "Top 10 Searches<BR><BR>\n";
			$this->resultid = $this->db->query("select * from top_searchs");
			if($this->db->num_rows($this->resultid) > 0) {
				 print "\t\t\t\t\t\t\t<TABLE border=0 width=50%>\n";
				while($res2 = $this->db->get_array($this->resultid)) {
					$url = rawurlencode($res2[searchfor]);
					print "\t\t\t\t\t\t\t\t<TR><TD><A href=\"$PAGES[result]?searchfor=$url&hits=20&dorm=1\">$res2[searchfor]</a></td><td align=right>$res2[stuff]</td></TR>\n";
				}
				print "\t\t\t\t\t\t\t</TABLE>\n";
			}
		}

		function print_last10() {
			global $PAGES;
			print "Last 10 Searches<BR><BR>\n";
			$this->resultid = $this->db->query("select searchfor, results from dormlogs order by id DESC limit 0,10");
			print "\t\t\t\t\t\t\t<TABLE border=0 width=280><TR ALIGN=CENTER>\n";
			print "\t\t\t\t\t\t\t\t<TR><TD>Query</TD><td>Results</td>";
			while($res2 = $this->db->get_array($this->resultid)) {
				$res2[searchfor] = strip_tags($res2[searchfor]);
				$url = rawurlencode($res2[searchfor]);
				print "\t\t\t\t\t\t\t\t<TR><TD><A href=\"$PAGES[result]?searchfor=$url&hits=20&dorm=1\">$res2[searchfor]</a></TD><td align=right>$res2[results]</td>";
				print "</TR>\n";
			}
			print "\t\t\t\t\t\t\t</TABLE>\n";
		}

		function print_top20_files() {
			$this->resultid = $this->db->query("select sum(files.size) as file_count, hosts.ip as ip, hosts.name as hostname from files,hosts where (hosts.id=files.host_id) group by files.host_id order by file_count desc limit 0,20");
			print "\t\t\t\t\t\t\t<TABLE border=1 width=50%><TR ALIGN=CENTER>\n";
			print "\t\t\t\t\t\t\t\t<TR><TD>Sharing Size:</TD><TD>Hostname:</TD></TR>";
			while($array = $this->db->get_array($this->resultid)) {
				print "\t\t\t\t\t\t\t\t<TR><TD>".number_format(($array[file_count] / (1024*1024*1024)), 2)."GB</TD><TD><a href=\"\\\\$array[ip]\">$array[hostname]</a></TD></TR>";
			}
			print "\t\t\t\t\t\t\t</TABLE>\n";
		}

		function display(){
			if (!$this->resultid) {
				$veiwlogs = "select * from $this->table";
				$this->resultid = $this->db->query($veiwlogs);
			}
			$numresult = $this->db->num_rows($this->resultid);
				print "
						<TABLE border=0>
							<TR ALIGN=CENTER>
								<TD><U>IP</U></TD>
								<TD><U>Browser</U></TD>";
			for($step = 0; $step < $numresult; $step++){
				$res2 = $this->db->get_array($this->resultid);
				print "
					<TR>
						<TD>$res2[ip]</TD>
						<TD>$res2[browserinfo]</TD>
					</TR>";
			}
			print "
				</TABLE><BR><BR>
			";
		}
	}
}
