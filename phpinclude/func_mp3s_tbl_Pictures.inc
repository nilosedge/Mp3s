<?php

global $FUNC_MP3S_TBL_PICTURES_INC;
if (!$FUNC_MP3S_TBL_PICTURES_INC) {
   $FUNC_MP3S_TBL_PICTURES_INC=1;

   include($INC[db]);

   class PICTURES {

      var $db, $resultid;
      var $table, $debug;

      function PICTURES($debug) {
         global $DBUSER, $DBNAME, $DBHOST, $DBPASSWD;

			$this->debug = $debug;
         $this->table = "Pictures";
         $this->db = new Database($debug);
         $this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
         $this->db->usedatabase($DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=$id"));
		}

		function insert_pic($uid, $data) {
			$save = $this->db->escape($data);
			$this->db->query("insert into $this->table (uid, fullsize) values($uid, '$save')");
		}

		function display_thumbs() {

			$res = $this->db->query("select id, thumb from $this->table where accepted = 1 order by id desc limit 0, 10");
			print "<BR>";
			while($array = $this->db->get_array($res)) {
				print "<a href=\"to_pic.php?size=small&id=$array[id]\"><img src=\"to_pic.php?size=thumb&id=$array[id]\" alt=\"Pic $array[id]\" border=0></a>";

			}

		}

		function to_pic($size, $id) {
			if($size != "thumb" && $size != "small" && $size != "fullsize") return 0;
			else {
				$array = $this->db->get_array($this->db->query("select $size from $this->table where id=\"$id\""));
				header("Content-Type: image/jpeg");
				header("Content-Disposition: \"inline; id=$file\"");
				//header("Content-Description: \"$file\"");
				header("Accept-Ranges: bytes");
				header("Content-Length: ".strlen($array[$size]));
				print $array[$size];
			}
		}
	
	} // end class PICTURES

} // end include protection

?>
