<?php


global $FUNC_TABLE_PLAYLIST_INC;
if(!$FUNC_TABLE_PLAYLIST_INC) {
	$FUNC_TABLE_PLAYLIST_INC = 1;
	
	include($INC[db]);

	class PLAYLIST {

		var $table, $debug;

		function PLAYLIST($debug) {
			global $DBNAME, $DBUSER, $DBHOST, $DBPASSWD;
			$this->debug = $debug;
			$this->table = "playlist";
			$this->db = new Database($debug);
			$this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
			if($debug) print "This is the db $DBNAME<BR>\n";
			$this->db->usedatabase($DBNAME);
		} // end constructor playlist

		function insert($file_id, $level=0) {
			$this->db->query("LOCK TABLES $this->table WRITE");
			$this->db->query("insert into $this->table set file_id=\"$file_id\", level=\"$level\"");
			$this->db->query("UNLOCK TABLES");
		} // end function insert

		function delete($file_id) {
			$this->db->query("LOCK TABLES $this->table WRITE");
			$this->db->query("delete from $this->table where id=\"$file_id\"");
			$this->db->query("UNLOCK TABLES");
		}	// end function delete

		function getlist() {
			$res = $this->db->query("select id,file_id from $this->table order by level desc, time");
			while($stuff = $this->db->get_array($res)) {
				$temp[$stuff[id]] = $stuff[file_id];
			} return $temp;
		} // end function getlist

		function level_up($id) { $this->db->query("update $this->table set level=(level+1) where id = $id"); }
		function level_down($id) { $this->db->query("update $this->table set level=(level-1) where id = $id"); }
		//function delete($id) { $this->db->query("delete from $this->table where id = $id"); }
		function i_like($id) { $this->db->query("insert into i_like (id, songname, size) select (id, songname, size) from mp3s where id = $id"); }

	}  // end class PLAYLIST
	
}  // end include protection

?>
