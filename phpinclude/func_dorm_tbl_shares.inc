<?php

global $FUNC_DORM_TBL_SHARES_INC;
if (!$FUNC_DORM_TBL_SHARES_INC) {
   $FUNC_DORM_TBL_SHARES_INC=1;

   include($INC[db]);

   class SHARES {

      var $db, $resultid;
      var $table, $debug;

      function SHARES($debug) {
         global $DORM_DBUSER, $DORM_DBNAME, $DORM_DBHOST, $DORM_DBPASSWD;

			$this->debug = $debug;
         $this->table = "shares";
         $this->db = new Database($debug);
         $this->db->connect($DORM_DBHOST, $DORM_DBUSER, $DORM_DBPASSWD);
         $this->db->usedatabase($DORM_DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=\"$id\""));
		}

		function get_share_count($id) {
			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where host_id=$id"));
			return $array[count];
		}

		function print_shares($host_id, $idval) {
			$res = $this->db->query("select * from $this->table where host_id = $host_id order by name");

			while($array = $this->db->get_array($res)) {
				for($i = 0; $i < $idval + 3; $i++) { print " "; }
				for($i = 0; $i < ($idval + 3) / 3; $i++) { print "&nbsp;"; }
				print "<nobr><a target=\"phpmain\" href=\"\"><a class=\"tblItem\" title=\"$array[name]\" target=\"phpmain\" href=\"\">$array[name]</a></nobr><br />\n";
			}

		}
	
	} // end class SHARES

} // end include protection

?>
