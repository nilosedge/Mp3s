<?php

global $FUNC_MP3S_TBL_MODERATORS_INC;
if (!$FUNC_MP3S_TBL_MODERATORS_INC) {
   $FUNC_MP3S_TBL_MODERATORS_INC=1;

   include($INC[db]);

   class MODERATORS {

      var $db, $resultid;
      var $table, $debug;

      function MODERATORS($debug) {
         global $DBUSER, $DBNAME, $DBHOST, $DBPASSWD;

			$this->debug = $debug;
         $this->table = "moderators";
         $this->db = new Database($debug);
         $this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
         $this->db->usedatabase($DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=$id"));
		}

		function get_info_userid($id) {
			$array = $this->db->get_array($this->db->query("select * from $this->table where user_id=$id"));
			if($array) {
				return $array;
			} else {
				return;
			}
		}

	} // end class MODERATORS

} // end include protection

?>
