<?php

global $FUNC_DORM_TBL_SKIPWORDS_INC;
if (!$FUNC_DORM_TBL_SKIPWORDS_INC) {
   $FUNC_DORM_TBL_SKIPWORDS_INC=1;

   include($INC[db]);

   class SKIPWORDS {

      var $db, $resultid;
      var $table, $debug;

      function SKIPWORDS($debug) {
         global $DORM_DBUSER, $DORM_DBNAME, $DORM_DBHOST, $DORM_DBPASSWD;
			$this->debug = $debug;
         $this->table = "skipwords";
         $this->db = new Database($debug);
         $this->db->connect($DORM_DBHOST, $DORM_DBUSER, $DORM_DBPASSWD);
         $this->db->usedatabase($DORM_DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=\"$id\""));
		}

		function chop_words($array) {
			for($i = 0; $i < count($array); $i++) {
				if($this->exists($array[$i])) {
					if($this->debug) print "This is a skip $array[$i]<BR>\n";
				} else {
					if($this->debug) print "This is not a skip word: $array[$i]<BR>\n";
					$myarray = $this->db->get_array($this->db->query("select id from words where word='$array[$i]'"));
					if($myarray[id]) $ret[] = $array[$i];
				}
			}
			return $ret;
		}

		function exists($word) {
			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where word=\"$word\""));
			return $array[count];
		}
	
	} // end class SKIPWORDS

} // end include protection

?>
