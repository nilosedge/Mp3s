<?php

global $FUNC_DORMINFO_INC;
if (!$FUNC_DORMINFO_INC){

	$FUNC_DORMINFO_INC=1;

	include($INC[db]);

	class DORMINFO {

	   var $db, $resultid;
		var $table;

	   function DORMINFO($debug){
			global $DORM_DBUSER, $DORM_DBNAME, $DORM_DBHOST, $DORM_DBPASSWD;

			$this->table = "info";	
	      $this->db = new Database($debug);
	      $this->db->connect($DORM_DBHOST, $DORM_DBUSER, $DORM_DBPASSWD);
	      $this->db->usedatabase($DORM_DBNAME);
	   }

		function get_lastest() {
			$array = $this->db->get_array($this->db->query("Select * from info order by id DESC"));
			return $array[info];
		}
	}
}

?>
