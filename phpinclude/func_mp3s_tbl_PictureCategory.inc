<?php

global $FUNC_MP3S_TBL_PICTURECATEGORY_INC;
if (!$FUNC_MP3S_TBL_PICTURECATEGORY_INC) {
   $FUNC_MP3S_TBL_PICTURECATEGORY_INC=1;

   include($INC[db]);

   class PICTURECATEGORY {

      var $db, $resultid;
      var $table, $debug;

      function PICTURECATEGORY($debug) {
         global $DBUSER, $DBNAME, $DBHOST, $DBPASSWD;

			$this->debug = $debug;
         $this->table = "PictureCategory";
         $this->db = new Database($debug);
         $this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
         $this->db->usedatabase($DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=$id"));
		}
	
	} // end class PICTURECATEGORY

} // end include protection

?>
