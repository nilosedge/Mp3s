<?php

global $FUNC_TABLE_UPDATES_INC;
if (!$FUNC_TABLE_UPDATES_INC){

	$FUNC_TABLE_UPDATES_INC=1;

	include($INC[db]);

	class UPDATES {

		var $db, $resultid, $table, $debug;

		function UPDATES($debug) {
			global $DBUSER, $DBHOST, $DBPASSWD, $DBNAME;
			$this->table = "updates";
			$this->db = new Database($debug);
			$this->debug = $debug;
			$this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
			$this->db->usedatabase($DBNAME);
		} // end class definition updates()

		function delete($id) {
			if($id) return $this->db->query("delete from $this->table where id = '$id'");
			else return 0;
		} // end function delete

		function insert($name, $data, $time) {
			$this->db->debug = 1;
			$this->db->query("insert into $this->table (name, updatenote, timestamp) values(\"$name\", \"$data\", \"$time\")");
			$this->db->debug = 0;
		} // end function insert

		function displaytable() {
			$counter=0;
			$query = "select * from $this->table order by id DESC";
			$this->resultid = $this->db->query($query);
			// $rows = $this->db->num_rows($this->resultid);
			print "	 <TABLE width=70% border=1>\n";
			while ($field = $this->db->get_field($this->resultid)) {
				$fields[$counter++] = $field->name;
				// print "	<TD>".$field->name."</TD>\n";
			}
			while ($stuff = $this->db->get_array($this->resultid)) {
				print "		<TR>\n		  <TD>".$stuff[$fields[3]]."</TD>\n";
				print "		  <TD>".$stuff[$fields[2]]."<P>".$stuff[$fields[1]]."<BR></TD>\n		</TR>\n";
			}
			print "	 </TABLE>\n";
		} // end function displaytable()
	} // end class updates
} // end include protection 

?>
