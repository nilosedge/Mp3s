<?php

global $FUNC_MP3S_TBL_MODERATED_INC;
if (!$FUNC_MP3S_TBL_MODERATED_INC) {
   $FUNC_MP3S_TBL_MODERATED_INC=1;

   include($INC[db]);

   class MODERATED {

      var $db, $resultid;
      var $table, $debug;

      function MODERATED($debug) {
         global $DBUSER, $DBNAME, $DBHOST, $DBPASSWD;

			$this->debug = $debug;
         $this->table = "moderated";
         $this->db = new Database($debug);
         $this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
         $this->db->usedatabase($DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=$id"));
		}

		function insert_mod($mod_id, $mes_id, $mv) {
			$this->db->query("insert into $this->table (mes_id, mod_id, modvalue) values($mes_id, $mod_id, $mv)");
		}

		function exists($mod_id, $mes_id) {
			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where mod_id=$mod_id and mes_id=$mes_id"));
			return $array[count];
		}

	} // end class MODERATED

} // end include protection

?>
