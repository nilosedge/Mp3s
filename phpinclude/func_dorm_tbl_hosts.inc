<?php

global $FUNC_DORM_TBL_HOSTS_INC;
if (!$FUNC_DORM_TBL_HOSTS_INC) {
   $FUNC_DORM_TBL_HOSTS_INC=1;

   include($INC[db]);

   class HOSTS {

      var $db, $resultid;
      var $table, $debug, $level;
		var $spaces;

      function HOSTS($debug) {
         global $DORM_DBUSER, $DORM_DBNAME, $DORM_DBHOST, $DORM_DBPASSWD;

			$this->debug = $debug;
         $this->table = "hosts";
         $this->db = new Database($debug);
         $this->db->connect($DORM_DBHOST, $DORM_DBUSER, $DORM_DBPASSWD);
         $this->db->usedatabase($DORM_DBNAME);
			$this->spaces = 0;
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=\"$id\""));
		}

		function print_hosts() {
			$share = new SHARES($this->debug);
			$res = $this->db->query("select * from $this->table order by name");
			$this->level = 1;
			$this->print_head($this->level, "Dorm");

			while($array = $this->db->get_array($res)) {

				$count = $share->get_share_count($array[id]);

				$this->print_parent(++$this->level, $array[name], $array[ip], $count);
				$this->print_child_start();
				$share->print_shares($array[id], $this->spaces);
				$this->print_child_end();
			}
		}

		function print_child_start() {
			$this->indent();
	   	$this->p("<div id=\"el".$this->level."Child\" class=\"child\" style=\"margin-bottom: 5px\">");
		}

		function print_child_end() {
			$this->p("</div>");
			$this->unindent();
		}

		function print_head($num, $name) {
			$this->indent();
    		$this->p("<div id=\"el$num"."Parent\" class=\"parent\" style=\"margin-bottom: 5px\">");
			$this->indent();
			$this->ps("<nobr><span class=\"heada\"><b>$name</b></span></nobr>");
			$this->unindent();
			$this->p("</div>");
			$this->unindent();
		}


		function print_parent($num, $name, $ip, $count) {
			$this->indent();
    		$this->p("<div id=\"el$num"."Parent\" class=\"parent\" style=\"margin-bottom: 5px\">");
			$this->indent();
			$this->ps("<nobr>$count <a class=\"item\" href=\"\" onclick=\"if (capable) {expandBase('el$num', true); return false;}\"><img name=\"imEx\" id=\"el$num"."Img\" src=\"images/plus.gif\" border=\"0\" alt=\"+\" /></a> <a class=\"item\" href=\"\" onclick=\"if (capable) {expandBase('el$num', false)}\"><span class=\"heada\">$name<bdo dir=\"ltr\">&nbsp;&nbsp;</bdo></span><span class=\"headaCnt\">$ip</span></a></nobr>");
			$this->unindent();
			$this->p("</div>");
			$this->unindent();
                   #<nobr><a class=\"item\" href=\"\"><span class=\"heada\"><b>$name</b></span></a></nobr>
		}

		function indent() { $this->spaces += 3; }
		function unindent() { $this->spaces -= 3; }

		function p($text) {
			for($i = 0; $i < $this->spaces; $i++) { print " "; }
			print "$text\n";
		}

		function ps($text) {
			for($i = 0; $i < $this->spaces; $i++) { print " "; }
			for($i = 0; $i < $this->spaces / 3; $i++) { print "&nbsp;"; }
			print "$text\n";
		}
	
	} // end class HOSTS

} // end include protection

?>
