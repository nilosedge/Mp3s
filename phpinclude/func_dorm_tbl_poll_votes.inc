<?php

global $FUNC_DORM_TBL_POLL_VOTES_INC;
if (!$FUNC_DORM_TBL_POLL_VOTES_INC) {
   $FUNC_DORM_TBL_POLL_VOTES_INC=1;

   include($INC[db]);

   class POLL_VOTES {

      var $db, $resultid;
      var $table, $debug;

      function POLL_VOTES($debug) {
         global $DORM_DBUSER, $DORM_DBNAME, $DORM_DBHOST, $DORM_DBPASSWD;

			$this->debug = $debug;
         $this->table = "poll_votes";
         $this->db = new Database($debug);
         $this->db->connect($DORM_DBHOST, $DORM_DBUSER, $DORM_DBPASSWD);
         $this->db->usedatabase($DORM_DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where id=\"$id\""));
		}

		function vote($ip, $qid, $aid) {
			if($this->exists($ip, $qid)) {
				return false;
			} else {
				$this->db->query("insert into $this->table (ip, qid, aid) values(\"$ip\", \"$qid\", \"$aid\")");
				$this->db->query("update poll_answers set votes=votes+1 where qid=\"$qid\" and id=\"$aid\"");
				return true;
			}
		}

		function exists($ip, $qid) {
			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where ip=\"$ip\" and qid=\"$qid\""));
			return $array[count];
		}

	} // end class POLL_VOTES

} // end include protection

?>
