<?php
	
global $FUNC_DB_MYSQL_INC;

if (!$FUNC_DB_MYSQL_INC) {
	$FUNC_DB_MYSQL_INC=1;

	class Database {
		var $connection_id, $debug;

		function Database($debug) {
			$this->debug = $debug;
			if ($debug) print "The debug level is $debug in func_db_mysql.inc<BR>\n";
		}

		function connect($hostname, $username, $password) {
			global $webhostname;
			$this->connection_id = mysql_connect($hostname, $username, $password);
			if($this->debug) print "We are connecting to $hostname with $username:$password<BR>\n";
			if ($this->connection_id) return $this->connection_id;
			else {
				if ($this->debug) print "Error in class Database: Can't connect to database<BR>\n";
				$body = "\n\nCannot connect to MySQL server on nilosplace.net\n\n";
				$this->errors("MySQL down", $body); 
				return 0;
			}
		}

		function errors($subject,$data) {
			global $ADMINEMAIL1, $ERROREMAIL;
			mail($ADMINEMAIL1, $subject, $data, "From: $ERROREMAIL\nContent-Type: text/html; charset=iso-8859-1");
		}

		function usedatabase($database) {
			if($this->debug) { print "This is the database we are trying to use: $database<BR>\n"; }
			$result_id = mysql_select_db($database, $this->connection_id);
			if ($result_id) return $result_id;
			else {
				if ($this->debug) {
					print "Error in class Database: Can't select database<BR>\n";
					print "This is the database we are trying to use: $database<BR>\n";
				} return 0;
			}
		}

		function query($querystring) {
			if ($this->debug) print "This is the query: $querystring<BR>\n";
			if (!$querystring) {
				if ($this->debug) print "There was no query passed to the query function<BR>\n";
				return 0;
			}
			$result_id = mysql_query($querystring, $this->connection_id);
			if ($result_id) return $result_id;
			else {
				if ($this->debug) {
					print "Error in class Database: No records found please try something else.<BR>\n";
					print "This means that there was no result_id.<BR>\n";
				} return 0;
			}
		} // end function query();

		function num_rows($resultid) { return mysql_num_rows($resultid); }
		function get_error() { return mysql_error($this->connection_id); }
		function get_row($resultid) { return mysql_fetch_row($resultid); }
		function get_field($resultid) { return mysql_fetch_field($resultid); }
		function get_array($resultid) { return mysql_fetch_array($resultid); }
		function get_object($resultid) { return mysql_fetch_object($resultid); }
		function get_assarray($resultid) { return mysql_fetch_array($resultid, MYSQL_ASSOC); } 
		function get_enuarray($resultid) { return mysql_fetch_array($resultid, MYSQL_NUM); }
		function escape($string) { return mysql_escape_string($string); }

	} # end class Database
} # if($FUNC_DB_INC)

?>
