<?php

global $FUNC_MP3_INC;
if(!$FUNC_MP3_INC) {
	$FUNC_MP3_INC=1;

	include($INC[db]);

	class MP3 {

		var $db, $resultid, $searchquery, $table; 
		var $searchstring, $searchby, $totalhits;
		var $hits, $start, $debug;

		function MP3($debug){
			global $DBNAME, $DBUSER, $DBHOST, $DBPASSWD;
			$this->table = "mp3s";
			$this->debug = $debug;
			$this->db = new Database($debug);
			$this->db->connect($DBHOST, $DBUSER, $DBPASSWD);
			$this->db->usedatabase($DBNAME);
		}

		function update( $id, $songname, $albumname, $genre, $artistname, $releaseyear, $size, $bitrate, $lyricsfile, $audiofile, $comment, $length) {
			if($id){
				$modifyquery = "update $this->table set songname=\"$songname\",albumname=\"$albumname\",genre=\"$genre\",artistname=\"$artistname\",releaseyear=\"$releaseyear\",size=\"$size\",bitrate=\"$bitrate\",lyricsfile=\"$lyricsfile\",audiofile=\"$audiofile\",comment=\"$comment\",length=\"$length\" where id=\"$id\"";
				if($this->debug) print "<BR><CENTER>$modifyquery</CENTER>";
				$this->resultid = $this->db->query($modifyquery);
			}
			else {
				$modifyquery = "insert into $this->table values(\"\",\"$songname\",\"$albumname\",\"$genre\",\"$artistname\",\"$releaseyear\",\"$size\",\"$bitrate\",\"$lyricsfile\",\"$audiofile\",\"$comment\",\"$length\")";
				if($this->debug) print "<BR><CENTER>$modifyquery</CENTER>";
				$this->resultid = $this->db->query($modifyquery);
			}
		}
	
		function delete($id){
			if($id) { 
				$deletequery = "DELETE FROM $this->table WHERE id='$id'";
				if($this->debug) print "<BR><CENTER>$deletequery</CENTER>";
				$this->resultid = $this->db->query($deletequery);
				return $this->resultid;
			}
			else return 0;
		}
	
		function randomdownload() {
			$temp = $this->db->get_array($this->db->query("select count(*) as count from $this->table"));
			$random = rand(1, $temp[count]);
			$array = $this->db->get_array($this->db->query("select *, concat(file_path, file_name) as audiofile from $this->table where id='$random'"));
			if(sendfile($array[audiofile])) return 1;
			return 0;
		}
	
	
		function download($id) {
			$array = $this->db->get_array($this->db->query("select concat(file_path, file_name) as audiofile from $this->table where id='$id'"));
			#print "$array[audiofile]<BR>\n";
			if (sendfile($array[audiofile])) return 1;
			return 0;
		}
	
		function get_id($filename) {
			return $this->db->get_array($this->db->query("select id from $this->table where file_name=\"$filename\""));
		}
	
		function get_info($id) {
			return $this->db->get_array($this->db->query("select *, concat(file_path, file_name) as audiofile from $this->table where id='$id'"));
		}

		function get_sorted_list($ids) {
			for($i = 0; $i < count($ids); $i++) {
				$query .= "id=$ids[$i] ";
				if($i < count($ids) - 1) $query .= "|| ";
			}
			$result = $this->db->query("select * from $this->table where ($query) order by file_name");
			for($i = 0; $array = $this->db->get_array($result); $i++) {
				$return[$i][id] = $array[id];
				$return[$i][size] = $array[size];
				$return[$i][filename] = $array[file_name];
			}
			return $return;
		}
	
		function search($searchstring, $searchby, $start, $hits) {
			$this->searchstring = $searchstring;
			$this->searchby = strtolower($searchby);
			if (!$start) $start = 0;
			if (!$hits) $hits = 10;
			$this->start = $start;
			$this->hits = $hits;

			$wordarray = split(" ", $searchstring);


			if ($this->searchby == "all") {
				$start_query = "select *, concat(file_path, file_name) as audiofile from $this->table where ";
				$middle_query = "(";
				for($i = 0; $i < count($wordarray); $i++) {
					$middle_query .= "(concat(file_path, file_name) like '%$wordarray[$i]%' or ";
					#$middle_query .= "songname like '%$wordarray[$i]%' or ";
					#$middle_query .= "albumname like '%$wordarray[$i]%' or ";
					#$middle_query .= "artistname like '%$wordarray[$i]%' or ";
					$middle_query .= "comment like '%$wordarray[$i]%')";
					if($i < (count($wordarray) - 1)) $middle_query .= " and ";
				}
				$middle_query .= ")";
			}
			else {
				$start_query = "select * from $this->table where ";
				$middle_query = "(";
				for($i = 0; $i < count($wordarray); $i++) {
					$middle_query .= " $this->searchby like '%$wordarray[$i]%' ";
					if($i < (count($wordarray) - 1)) $middle_query .= " and ";
				}
				$middle_query .= ")";
			}

			$this->searchquery = $start_query.$middle_query;
			if($this->debug) print "This is the query in search: ".$this->searchquery."<BR>\n";
			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where".$middle_query));
			$this->totalhits = $array[count];
			$this->searchquery .= " order by artistname";
			$this->resultid = $this->db->query($this->searchquery." limit $this->start, $this->hits");
			return $this->resultid;
		}
	
		function display($layout) {
			global $searchfor, $hits, $start, $PAGES, $admin;
			$searchfor = urlencode($searchfor);
			if (!$layout) $layout = "long";
			if ($layout == "long") {
				$numresult = $this->db->num_rows($this->resultid);
				print "<!-- Starting Table Generation -->\n";
				if ($numresult > 0) {
					if (($this->start + $this->hits) > ($this->start + $numresult)) $temp = $this->start + $numresult;
					else $temp = $this->start + $this->hits;
					print "Mp3 results ".($this->start + 1)." - ".($temp)." of $this->totalhits total Matches.<BR><BR>\n";
					print "<form action=\"$PAGES[result]?hits=$this->hits&start=$this->start&searchfor=$searchfor\" method=post>
       <TABLE width=100% border=1 cellpadding=1 cellspacing=1>
        <TR ALIGN=CENTER>
          <TD><U>Download</U></TD>
          <TD><U>Radio Playlist</U></TD>
          <TD><U>Artist</U></TD>
          <TD><U>Album</U></TD>
          <TD><U>Song</U></TD>
          <TD><U>Genre</U></TD>
          <TD><U>Bitrate</U></TD>
          <TD><U>Size</U></TD>
          <TD><U>Release Year</U></TD>
          <TD><U>Length</U></TD>
          <TD><U>Comment</U></TD>
        </TR>";
					for($step = 0; $step < $numresult; $step++){
						$res1 = $this->db->get_array($this->resultid);
						$res1[length] = date("i:s",$res1[length]);
						if(!$res1[comment]) $res1[comment] = "&nbsp;";
						print "
        <TR>
          <TD><A HREF=\"$PAGES[result]?download=$res1[id]\">Download</A></TD>";
			if($admin) print "
          <TD align=center><input type=checkbox name=\"playlist[$res1[id]]\"></TD>
			";
			print "
          <TD>$res1[artistname]</TD>
          <TD>$res1[albumname]</TD>
          <TD>$res1[songname]</TD>
          <TD ALIGN=RIGHT>$res1[genre]</TD>
          <TD ALIGN=RIGHT>$res1[bitrate]</TD>
          <TD ALIGN=RIGHT>$res1[size]</TD>
          <TD ALIGN=RIGHT>$res1[releaseyear]</TD>
          <TD ALIGN=RIGHT>$res1[length]</TD>
          <TD>$res1[comment]</TD>
        </TR>\n";
					}
			print"
        <TR ALIGN=CENTER>
          <TD>&nbsp;</TD>";
			if($admin) print "
          <TD><input type=submit name=button value=\"Add\"></TD>
			";
			print "
          <TD colspan=9>&nbsp;</TD>
        </TR>";
					print "</form>\n       </TABLE>\n";
					$this->displaynumbers();
				}
				else print "Sorry was unable to find anything that matched your request<BR>\n";
				print "<!-- Ending Table Generation -->\n";
			}
			else print "<FONT COLOR=#00FF00>No layout or unknown layout: $layout";
		} // end function display 
	
		function displaynumbers() {
			global $PHP_SELF;
			global $searchfor;
			global $PAGES;
			global $COLORS;
			print "       <BR><BR>\n       <CENTER>\n";
			print "       <TABLE>\n         <TR>\n";
				if ($this->totalhits > $this->hits) {
					if ($this->start > 0) print "           <TD><A HREF=\"$PAGES[result]?searchby=".$this->searchby."&searchfor=".$searchfor."&start=".($this->start - $this->hits)."&hits=$this->hits\"><--Prev</A></TD>\n";
					else print "           <TD><FONT color=000000><--Prev</FONT></TD>\n";
					$counter = 0;
					if($this->debug > 1) print "This is the start $this->start<BR>\n";
					for($i = 0; $i < $this->totalhits && $counter < 30; $i += $this->hits) {
						$counter++;
						$color = $COLORS[green][link];
						print "<td>\n";
						if($i == $this->start) print "           <font color=$color size=+4><b>$counter</b></font>";
						else {
							print "           <A HREF=\"$PAGES[result]?searchby=".$this->searchby."&searchfor=".$searchfor."&hits=$this->hits&start=$i\">";
							$string = "$counter";
							for ($q = 0; $q < strlen($string); $q++) {
								print "<IMG SRC=\"pix/$string[$q].jpg\" border=0 ALT=\"$counter\">";
							}
							print "</a>\n";
						}
						print "&nbsp;</TD>\n";
					}
					if ($this->totalhits > ($this->hits + $this->start)) print "           <TD><A HREF=\"$PAGES[result]?searchby=".$this->searchby."&searchfor=".$searchfor."&start=".($this->start + $this->hits)."&hits=$this->hits\" ALT=\"Next\">Next--></A></TD>\n";
					else print "           <TD><FONT color=000000>Next--></FONT></TD>\n";
				}
			print "         </TR>\n       </TABLE>";
			print "
        <table border=0 cellspacing=0 cellpadding=0>
          <tr><td>
              <FORM action=\"$PHP_SELF\" METHOD=POST>
                <INPUT TYPE=TEXT NAME=\"searchfor\" size=31 maxlength=256 value=\"$this->searchstring\"> 
                <INPUT name=search type=submit value=\"Mp3 Search\">
                <INPUT type=hidden name=hits value=\"$this->hits\">
              </FORM>
          </td></tr>
        </table>\n";
		} // end function displaynumbers
	} // end class MP3
} // end include protection
